package cn.sunxiansheng.intelligent.analysis.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 智能分析自动配置类
 *
 * @Author sun
 * @Create 2025/1/1 19:27
 * @Version 1.0
 */
@Configuration
@Slf4j
public class IntelligentAnalysisAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("IntelligentAnalysisAutoConfiguration has been loaded successfully!");
    }
}