package cn.sunxiansheng.elk.config;

import cn.sunxiansheng.elk.config.properties.ELKProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: ELK自动配置类
 *
 * @Author sun
 * @Create 2024/12/31 13:14
 * @Version 1.0
 */
@Configuration
@EnableConfigurationProperties({ELKProperties.class}) // 启用配置类
@Slf4j
public class ELKAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("ELKAutoConfiguration has been loaded successfully!");
    }
}