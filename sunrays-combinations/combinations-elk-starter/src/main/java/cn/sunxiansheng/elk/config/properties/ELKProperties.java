package cn.sunxiansheng.elk.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: ELK 属性
 *
 * @Author sun
 * @Create 2025/1/10 22:32
 * @Version 1.0
 */
@ConfigurationProperties(prefix = "sun-rays.elk")
@Data
public class ELKProperties {

    @Data
    public static class Logstash {

        /**
         * LogStash的主机
         */
        private String host;

        /**
         * LogStash的端口
         */
        private Integer port;
    }

    /**
     * 当前项目模块名(从仓库中最顶级的模块开始) 例如：sunrays-framework/sunrays-demo/common-log4j2-starter-demo
     */
    private String module = "defaultModule";

    /**
     * Logstash的配置
     */
    private Logstash logstash = new Logstash();
}