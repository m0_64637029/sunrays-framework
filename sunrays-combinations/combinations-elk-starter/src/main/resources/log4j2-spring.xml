<?xml version="1.0" encoding="UTF-8"?>
<!-- Log4j2 配置的根元素 -->
<Configuration status="INFO" monitorInterval="30">
<!--
    status="INFO"：设置 Log4j2 内部日志的输出级别为 INFO，用于调试配置问题。
    monitorInterval="30"：Log4j2 将每 30 秒检查一次配置文件的变化，实现热加载配置。
-->

<!-- TRACE < DEBUG < INFO < WARN < ERROR < FATAL -->

<!-- 定义全局属性，可在整个配置文件中使用 -->
<Properties>
    <!-- logstash 的 SocketAppender 配置 -->
    <Property name="socket.host">${sys:socket.host}</Property>
    <Property name="socket.port">${sys:socket.port}</Property>

    <!-- 日志存储的根目录 -->
    <Property name="LOG_HOME">${sys:log.home}</Property>

    <!-- 日志文件的名称前缀 -->
    <Property name="LOG_NAME">sunrays-framework</Property>

    <!-- 控制台日志输出格式，带颜色 -->
    <Property name="CONSOLE_LOG_PATTERN">
        %style{%d{yyyy-MM-dd HH:mm:ss.SSS}}{green} %style{[%t]}{blue} %highlight{%p}{FATAL=red blink, ERROR=red, WARN=yellow, INFO=green, DEBUG=cyan, TRACE=magenta} %style{[PFTID:%X{traceId}]}{magenta} %style{[Module:${sys:log.module}]}{yellow} %style{%logger{36}}{cyan} - %msg%n%throwable
    </Property>

    <!-- 文件日志输出格式，不带颜色 -->
    <Property name="FILE_LOG_PATTERN">
        %d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %-5p [PFTID:%X{traceId}] [Module:${sys:log.module}] %logger{36} - %msg%n%throwable
    </Property>
</Properties>

<!-- 定义日志输出目的地（Appender） -->
<Appenders>
    <!-- 定义 SocketAppender -->
    <Socket name="RealTimeSocketAppender" host="${socket.host}" port="${socket.port}">
        <!-- 分隔符为 XYZ123DELIMITERXYZ123 -->
        <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} XYZ123DELIMITERXYZ123 [%t] XYZ123DELIMITERXYZ123 %-5p XYZ123DELIMITERXYZ123 [PFTID:%X{traceId}] XYZ123DELIMITERXYZ123 [Module:${sys:log.module}] XYZ123DELIMITERXYZ123 %logger{36} XYZ123DELIMITERXYZ123 %msg XYZ123DELIMITERXYZ123 %n%throwable"/>
        <!-- 添加日志级别过滤器只要info以及以上的日志 -->
        <Filters>
            <ThresholdFilter level="INFO" onMatch="ACCEPT" onMismatch="DENY" />
        </Filters>
    </Socket>

    <!-- 控制台输出 -->
    <Console name="Console" target="SYSTEM_OUT">
        <!-- 使用带颜色的布局模式 -->
        <PatternLayout pattern="${CONSOLE_LOG_PATTERN}"/>
        <!-- 设置日志级别过滤器，只允许 INFO 及以上级别 -->
        <ThresholdFilter level="INFO" onMatch="ACCEPT" onMismatch="DENY"/>
    </Console>

    <!-- INFO 级别日志文件输出 -->
    <RollingFile name="InfoFile"
                 fileName="${LOG_HOME}/unarchived/info/${date:yyyy-MM-dd}/current.log"
                 filePattern="${LOG_HOME}/archived/info/${date:yyyy-MM-dd}/${LOG_NAME}-info-%d{yyyy-MM-dd-HH-mm-ss}-%i.log.gz">
        <!-- 使用不带颜色的布局模式 -->
        <PatternLayout pattern="${FILE_LOG_PATTERN}"/>
        <Policies>
            <!-- 使用 Cron 表达式，每2小时归档一次 -->
            <CronTriggeringPolicy schedule="0 0 0/2 * * ?"/>
            <!-- 文件大小超过 50MB 时归档 -->
            <SizeBasedTriggeringPolicy size="50MB"/>
        </Policies>
        <DefaultRolloverStrategy>
            <!-- 删除超过7天的归档日志（Log4j2 2.7 及以上版本的特性）-->
            <Delete basePath="${LOG_HOME}/archived/info" maxDepth="2">
                <IfFileName glob="*.log.gz"/>                        <!-- 匹配 .log.gz 的文件 -->
                <IfLastModified age="7d"/>                           <!-- 文件修改时间超过7天 -->
            </Delete>
        </DefaultRolloverStrategy>
        <Filters>
            <!-- 只接受 INFO 及以上级别的日志 -->
            <ThresholdFilter level="INFO" onMatch="ACCEPT" onMismatch="DENY"/>
        </Filters>
    </RollingFile>

    <!-- WARN 级别日志文件输出 -->
    <RollingFile name="WarnFile"
                 fileName="${LOG_HOME}/unarchived/warn/${date:yyyy-MM-dd}/current.log"
                 filePattern="${LOG_HOME}/archived/warn/${date:yyyy-MM-dd}/${LOG_NAME}-warn-%d{yyyy-MM-dd-HH-mm-ss}-%i.log.gz">
        <PatternLayout pattern="${FILE_LOG_PATTERN}"/>
        <Policies>
            <CronTriggeringPolicy schedule="0 0 0/2 * * ?"/>
            <SizeBasedTriggeringPolicy size="50MB"/>
        </Policies>
        <DefaultRolloverStrategy>
            <Delete basePath="${LOG_HOME}/archived/warn" maxDepth="2">
                <IfFileName glob="*.log.gz"/>
                <IfLastModified age="7d"/>
            </Delete>
        </DefaultRolloverStrategy>
        <Filters>
            <!-- 只接受 WARN 及以上级别的日志 -->
            <ThresholdFilter level="WARN" onMatch="ACCEPT" onMismatch="DENY"/>
        </Filters>
    </RollingFile>

    <!-- ERROR 级别日志文件输出 -->
    <RollingFile name="ErrorFile"
                 fileName="${LOG_HOME}/unarchived/error/${date:yyyy-MM-dd}/current.log"
                 filePattern="${LOG_HOME}/archived/error/${date:yyyy-MM-dd}/${LOG_NAME}-error-%d{yyyy-MM-dd-HH-mm-ss}-%i.log.gz">
        <PatternLayout pattern="${FILE_LOG_PATTERN}"/>
        <Policies>
            <CronTriggeringPolicy schedule="0 0 0/2 * * ?"/>
            <SizeBasedTriggeringPolicy size="50MB"/>
        </Policies>
        <DefaultRolloverStrategy>
            <Delete basePath="${LOG_HOME}/archived/error" maxDepth="2">
                <IfFileName glob="*.log.gz"/>
                <IfLastModified age="7d"/>
            </Delete>
        </DefaultRolloverStrategy>
        <Filters>
            <!-- 只接受 ERROR 及以上级别的日志 -->
            <ThresholdFilter level="ERROR" onMatch="ACCEPT" onMismatch="DENY"/>
        </Filters>
    </RollingFile>

    <!-- 全部级别日志文件输出（包含 DEBUG 及以上） -->
    <RollingFile name="AllFile"
                 fileName="${LOG_HOME}/unarchived/all/${date:yyyy-MM-dd}/current.log"
                 filePattern="${LOG_HOME}/archived/all/${date:yyyy-MM-dd}/${LOG_NAME}-all-%d{yyyy-MM-dd-HH-mm-ss}-%i.log.gz">
        <PatternLayout pattern="${FILE_LOG_PATTERN}"/>
        <Policies>
            <CronTriggeringPolicy schedule="0 0 0/2 * * ?"/>
            <SizeBasedTriggeringPolicy size="50MB"/>
        </Policies>
        <DefaultRolloverStrategy>
            <Delete basePath="${LOG_HOME}/archived/all" maxDepth="2">
                <IfFileName glob="*.log.gz"/>
                <IfLastModified age="7d"/>
            </Delete>
        </DefaultRolloverStrategy>
        <!-- 不添加 ThresholdFilter，接受所有级别的日志 -->
    </RollingFile>
</Appenders>

<!-- 配置日志记录器（Logger），定义日志的输出规则和级别 -->
<Loggers>
    <!--
        以下是针对特定模块的日志配置，目前被注释掉了。
        可以根据需要取消注释，定制化模块的日志级别和输出。
    -->

    <!-- 配置指定模块的异步日志 -->
    <!--
    <AsyncLogger name="cn.sunxiansheng" level="DEBUG" additivity="false" includeLocation="false">
        <AppenderRef ref="Console"/>
    </AsyncLogger>
    -->
    <!--
        name="cn.sunxiansheng"：指定包名或类名，针对该模块进行配置。
        level="DEBUG"：设置日志级别为 DEBUG，记录 DEBUG 及以上级别的日志。
        additivity="false"：不向父 Logger 传递，防止日志重复输出。
        includeLocation="false"：不包含代码位置信息，提高性能。
        <AppenderRef ref="Console"/>：将日志输出到 Console Appender。
    -->

    <!-- 配置其他特定模块的日志 -->
    <!--
    <Logger name="com.moduleA" level="INFO" additivity="false">
        <AppenderRef ref="Console"/>
    </Logger>
    -->
    <!--
        针对 com.moduleA 包，设置日志级别为 INFO，日志只输出到 Console，不向上级传播。
    -->

    <!-- 根日志记录器，处理未被其他 Logger 捕获的日志 -->
    <Root level="DEBUG">
        <!-- 引用之前定义的所有 Appender -->
        <AppenderRef ref="Console"/>
        <AppenderRef ref="InfoFile"/>
        <AppenderRef ref="WarnFile"/>
        <AppenderRef ref="ErrorFile"/>
        <AppenderRef ref="AllFile"/>
        <!-- 实时写入logstash -->
        <AppenderRef ref="RealTimeSocketAppender"/>
    </Root>
    <!--
        level="DEBUG"：设置根日志级别为 DEBUG，记录 DEBUG 及以上级别的日志。
        所有未被特定 Logger 处理的日志，都会按照根日志器的配置输出。
    -->
</Loggers>
</Configuration>
