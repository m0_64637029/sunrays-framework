package cn.sunxiansheng.quickstart.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 快速入门的自动配置类
 *
 * @Author sun
 * @Create 2025/1/17 19:21
 * @Version 1.0
 */
@Configuration
@Slf4j
public class QuickStartAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("QuickStartAutoConfiguration has been loaded successfully!");
    }
}