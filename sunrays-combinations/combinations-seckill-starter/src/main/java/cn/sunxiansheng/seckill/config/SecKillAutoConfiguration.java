package cn.sunxiansheng.seckill.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 秒杀 自动配置类
 *
 * @Author sun
 * @Create 2025/1/8 14:31
 * @Version 1.0
 */
@Configuration
@Slf4j
public class SecKillAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("SecKillAutoConfiguration has been loaded successfully!");
    }
}