package cn.sunxiansheng.wx.login.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 微信登录自动配置类
 *
 * @Author sun
 * @Create 2025/1/13 16:11
 * @Version 1.0
 */
@Configuration
@Slf4j
public class WxLoginAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("WxLoginAutoConfiguration has been loaded successfully!");
    }
}