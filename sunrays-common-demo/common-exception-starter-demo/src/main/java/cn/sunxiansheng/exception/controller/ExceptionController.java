package cn.sunxiansheng.exception.controller;

import cn.sunxiansheng.exception.CustomException;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Description: 测试异常处理
 *
 * @Author sun
 * @Create 2024/10/28 17:41
 * @Version 1.0
 */
@RestController
public class ExceptionController {

    @Data
    static class User {
        @NotBlank(message = "用户名不能为空")
        private String name;
        @NotNull(message = "年龄不能为空")
        private Integer age;
    }

    /**
     * 测试异常
     *
     * @return
     */
    @RequestMapping("/testException")
    public String testException() {
        int i = 1 / 0;
        return "testException";
    }

    /**
     * 测试参数校验异常
     *
     * @param user
     * @return
     */
    @RequestMapping("/testMethodArgumentNotValidException")
    public String testMethodArgumentNotValidException(@RequestBody @Valid User user) {
        int i = 1 / 0;
        return "testException";
    }

    /**
     * 测试自定义异常
     *
     * @return
     */
    @RequestMapping("/testCustomException")
    public String testCustomException() {
        throw new CustomException(111, "自定义异常");
    }
}