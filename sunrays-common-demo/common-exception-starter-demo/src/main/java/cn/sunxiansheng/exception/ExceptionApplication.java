package cn.sunxiansheng.exception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 异常处理启动类
 *
 * @Author sun
 * @Create 2024/10/28 17:32
 * @Version 1.0
 */
@SpringBootApplication
public class ExceptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExceptionApplication.class, args);
    }
}