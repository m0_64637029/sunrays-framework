package cn.sunxiansheng.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: Redis启动类
 *
 * @Author sun
 * @Create 2024/11/15 20:56
 * @Version 1.0
 */
@SpringBootApplication
public class RedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }
}