package cn.sunxiansheng.redis.controller;

import cn.sunxiansheng.redis.utils.RSortedSet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * Description: 测试 RSortedSet 工具类
 *
 * @Author sun
 * @Create 2024/11/16
 * @Version 1.0
 */
@RestController
@RequestMapping("/rsortedset")
public class RSortedSetController {

    @Autowired
    private RSortedSet rSortedSet;

    // 自定义类型
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CustomValue {
        private Long id;
        private String name;
    }

    @PostMapping("/add")
    public Boolean add(@RequestParam String key, @RequestBody CustomValue value, @RequestParam double score) {
        return rSortedSet.add(key, value, score);
    }

    @PostMapping("/addBatch")
    public Long addBatch(@RequestParam String key, @RequestBody Set<RSortedSet.DataScore<CustomValue>> dataScores) {
        return rSortedSet.addBatch(key, dataScores);
    }

    @PostMapping("/remove")
    public Long remove(@RequestParam String key, @RequestBody Set<CustomValue> values) {
        return rSortedSet.remove(key, values);
    }

    @PostMapping("/removeRangeByScore")
    public Long removeRangeByScore(@RequestParam String key, @RequestParam double min, @RequestParam double max) {
        return rSortedSet.removeRangeByScore(key, min, max);
    }

    @PostMapping("/changeByDelta")
    public Double changeByDelta(@RequestParam String key, @RequestBody CustomValue value, @RequestParam double delta) {
        return rSortedSet.changeByDelta(key, value, delta);
    }

    @GetMapping("/reverseRangeWithScores")
    public Set<ZSetOperations.TypedTuple<CustomValue>> reverseRangeWithScores(@RequestParam String key) {
        return rSortedSet.reverseRangeWithScores(key, CustomValue.class);
    }

    @GetMapping("/score")
    public Double score(@RequestParam String key, @RequestBody CustomValue value) {
        return rSortedSet.score(key, value);
    }

    @GetMapping("/reverseRank")
    public Long reverseRank(@RequestParam String key, @RequestBody CustomValue value) {
        return rSortedSet.reverseRank(key, value);
    }

    @GetMapping("/zCard")
    public Long zCard(@RequestParam String key) {
        return rSortedSet.zCard(key);
    }

    @GetMapping("/count")
    public Long count(@RequestParam String key, @RequestParam double min, @RequestParam double max) {
        return rSortedSet.count(key, min, max);
    }
}