package cn.sunxiansheng.redis.controller;

import cn.sunxiansheng.redis.utils.RSet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.Set;

/**
 * Description: RSet的测试Controller
 *
 * @Author sun
 * @Create 2024/11/15
 * @Version 1.0
 */
@RestController
@RequestMapping("/rset")
public class RSetController {

    @Autowired
    private RSet rSet;

    /**
     * 静态内部类表示简单的自定义对象
     */
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Element implements Serializable {
        private Long id;
        private String name;
    }

    /**
     * 添加一个元素
     */
    @PostMapping("/add")
    public Long add(@RequestParam String key, @RequestBody Element element) {
        return rSet.add(key, element);
    }

    /**
     * 批量添加元素
     */
    @PostMapping("/addBatch")
    public Long addBatch(@RequestParam String key, @RequestBody Set<Element> elements) {
        return rSet.addBatch(key, elements);
    }

    /**
     * 移除元素
     */
    @PostMapping("/remove")
    public Long remove(@RequestParam String key, @RequestBody Set<Element> elements) {
        return rSet.remove(key, elements);
    }

    /**
     * 获取集合
     */
    @GetMapping("/members")
    public Set<Element> members(@RequestParam String key) {
        return rSet.members(key, Element.class);
    }

    /**
     * 判断是否包含某元素
     */
    @GetMapping("/isMember")
    public Boolean isMember(@RequestParam String key, @RequestBody Element element) {
        return rSet.isMember(key, element);
    }

    /**
     * 获取集合大小
     */
    @GetMapping("/size")
    public Long size(@RequestParam String key) {
        return rSet.size(key);
    }
}