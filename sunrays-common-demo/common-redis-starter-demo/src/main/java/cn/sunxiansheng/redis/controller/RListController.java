package cn.sunxiansheng.redis.controller;

import cn.sunxiansheng.redis.utils.RList;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 测试 RList 工具类的 Controller
 *
 * @Author sun
 * @Create 2024/11/14 16:00
 * @Version 1.0
 */
@RestController
@RequestMapping("/rlist")
public class RListController {

    @Autowired
    private RList rList;

    /**
     * 左侧添加单个值
     */
    @PostMapping("/leftPush")
    public String leftPush(@RequestParam String key, @RequestBody User user) {
        rList.leftPush(key, user);
        return "User added to the left of the list.";
    }

    /**
     * 左侧批量添加多个值
     */
    @PostMapping("/leftPushAll")
    public String leftPushAll(@RequestParam String key, @RequestBody List<User> users) {
        rList.leftPushAll(key, users);
        return "Users added to the left of the list.";
    }

    /**
     * 右侧添加单个值
     */
    @PostMapping("/rightPush")
    public String rightPush(@RequestParam String key, @RequestBody User user) {
        rList.rightPush(key, user);
        return "User added to the right of the list.";
    }

    /**
     * 右侧批量添加多个值
     */
    @PostMapping("/rightPushAll")
    public String rightPushAll(@RequestParam String key, @RequestBody List<User> users) {
        rList.rightPushAll(key, users);
        return "Users added to the right of the list.";
    }

    /**
     * 根据索引设置List中的值
     */
    @PutMapping("/set")
    public String set(@RequestParam String key, @RequestParam int index, @RequestBody User user) {
        rList.set(key, index, user);
        return "User updated at index " + index;
    }

    /**
     * 获取List中的所有数据
     */
    @GetMapping("/rangeAll")
    public List<User> rangeAll(@RequestParam String key) {
        return rList.rangeAll(key, User.class);
    }

    /**
     * 根据索引获取List中的元素
     */
    @GetMapping("/getElementByIndex")
    public User getElementByIndex(@RequestParam String key, @RequestParam int index) {
        return rList.getElementByIndex(key, index, User.class);
    }

    /**
     * 获取List的长度
     */
    @GetMapping("/size")
    public Long size(@RequestParam String key) {
        return rList.size(key);
    }

    /**
     * 从右侧弹出元素
     */
    @DeleteMapping("/popRight")
    public User popRight(@RequestParam String key) {
        return rList.popRight(key, User.class);
    }

    /**
     * 从左侧弹出元素
     */
    @DeleteMapping("/popLeft")
    public User popLeft(@RequestParam String key) {
        return rList.popLeft(key, User.class);
    }

    /**
     * 根据值移除指定数量的匹配元素
     */
    @DeleteMapping("/removeByValue")
    public String removeByValue(@RequestParam String key, @RequestParam int count, @RequestBody User user) {
        Long removedCount = rList.removeByValue(key, count, user);
        return removedCount + " matching users removed.";
    }
}


/**
 * 简单的自定义类型 User
 *
 * @Author sun
 * @Create 2024/11/14 15:50
 * @Version 1.0
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
class User implements java.io.Serializable {
    private String id;
    private String name;
}