package cn.sunxiansheng.consumer.error;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Description: 错误消息监听器
 *
 * @Author sun
 * @Create 2024/12/31 20:32
 * @Version 1.0
 */
@Component
@Slf4j
public class ErrorListener {

    @RabbitListener(queues = "error.queue")
    public void errorListener(Message message) {
        // 解析错误信息
        ErrorMessageHandler.handleErrorMessage("error.queue", message);
    }
}