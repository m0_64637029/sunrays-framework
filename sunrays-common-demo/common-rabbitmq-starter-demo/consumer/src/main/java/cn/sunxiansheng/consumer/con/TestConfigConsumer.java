package cn.sunxiansheng.consumer.con;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Description: 测试消费者
 *
 * @Author sun
 * @Create 2024/12/31 19:03
 * @Version 1.0
 */
@Component
@Slf4j
public class TestConfigConsumer {

    @RabbitListener(queues = "fanout.queue.test")
    public void receive(String message) {
        log.info("接收到的消息：{}", message);
        int i = 1 / 0;
    }
}