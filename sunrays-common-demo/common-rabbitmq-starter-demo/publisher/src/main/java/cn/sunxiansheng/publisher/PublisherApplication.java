package cn.sunxiansheng.publisher;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 发布者启动类
 *
 * @Author sun
 * @Create 2024/12/31 21:36
 * @Version 1.0
 */
@SpringBootApplication
@Slf4j
public class PublisherApplication {

    public static void main(String[] args) {
        SpringApplication.run(PublisherApplication.class, args);
    }
}