package cn.sunxiansheng.minio.controller;

import cn.sunxiansheng.minio.utils.MinioUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description: Minio测试Controller
 *
 * @Author sun
 * @Create 2024/11/12 23:34
 * @Version 1.0
 */
@RestController
public class MinioController {

    @Resource
    private MinioUtil minioUtil;

    /**
     * 检查存储桶是否存在
     */
    @GetMapping("/bucketExists")
    public boolean bucketExists(@RequestParam String bucketName) {
        return minioUtil.bucketExists(bucketName);
    }

    /**
     * 列出所有存储桶的名字
     */
    @GetMapping("/listBucketNames")
    public List<String> listBucketNames() {
        return minioUtil.listBucketNames();
    }

    /**
     * 创建存储桶
     */
    @PostMapping("/makeBucket")
    public String makeBucket(@RequestParam String bucketName) {
        minioUtil.makeBucket(bucketName);
        return "Bucket " + bucketName + " created successfully!";
    }

    /**
     * 删除空的存储桶
     */
    @DeleteMapping("/removeBucket")
    public String removeBucket(@RequestParam String bucketName) {
        minioUtil.removeBucket(bucketName);
        return "Bucket " + bucketName + " removed successfully!";
    }

    /**
     * 上传文件并返回预览和下载链接
     */
    @PostMapping("/upload")
    public List<String> uploadFile(@RequestParam MultipartFile file, @RequestParam String bucketName) {
        return minioUtil.putObject(file, bucketName);
    }

    /**
     * 下载文件到指定路径
     */
    @GetMapping("/download")
    public String downloadFile(@RequestParam String bucketName,
                               @RequestParam String objectName,
                               @RequestParam String fileName) {
        minioUtil.downloadObject(bucketName, objectName, fileName);
        return "File " + objectName + " downloaded to " + fileName;
    }

    /**
     * 删除文件或文件夹
     */
    @DeleteMapping("/removeObject")
    public String removeObjectOrFolder(@RequestParam String bucketName, @RequestParam String prefix) {
        return minioUtil.removeObjectOrFolder(bucketName, prefix);
    }
}