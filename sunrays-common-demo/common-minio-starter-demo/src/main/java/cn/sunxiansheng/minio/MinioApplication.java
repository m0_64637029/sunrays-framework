package cn.sunxiansheng.minio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: Minio启动类
 *
 * @Author sun
 * @Create 2024/11/12 23:31
 * @Version 1.0
 */
@SpringBootApplication
public class MinioApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinioApplication.class, args);
    }
}