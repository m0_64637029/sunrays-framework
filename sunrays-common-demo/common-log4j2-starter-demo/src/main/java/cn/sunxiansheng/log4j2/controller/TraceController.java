package cn.sunxiansheng.log4j2.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

/**
 * Description: 测试链路追踪
 *
 * @Author sun
 * @Create 2024/10/24 17:50
 * @Version 1.0
 */
@Slf4j
@RestController
public class TraceController {

    @Resource
    private Executor customThreadPool;

    @RequestMapping("/trace")
    public String trace() {
        // 使用 CompletableFuture 提交异步任务，并确保上下文传递
        for (int i = 0; i < 10; i++) {
            CompletableFuture.runAsync(() -> {
                // 打印当前线程的 TraceId
                log.info("trace");
            }, customThreadPool);
        }
        // 返回响应结果
        return "Trace initiated successfully";
    }
}