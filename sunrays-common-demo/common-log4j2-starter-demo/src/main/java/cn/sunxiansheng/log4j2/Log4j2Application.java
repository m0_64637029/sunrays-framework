package cn.sunxiansheng.log4j2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: Log4j2Application
 *
 * @Author sun
 * @Create 2024/10/24 17:22
 * @Version 1.0
 */
@SpringBootApplication
public class Log4j2Application {

    public static void main(String[] args) {
        SpringApplication.run(Log4j2Application.class, args);
    }
}