package cn.sunxiansheng.log4j2.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Description: 线程池配置
 *
 * @Author sun
 * @Create 2025/1/1 12:23
 * @Version 1.0
 */
@Configuration
public class CustomThreadPoolConfig {

    @Bean(name = "customThreadPool")
    public Executor customThreadPoolExecutor() {
        int corePoolSize = 5;
        int maximumPoolSize = 10;
        long keepAliveTime = 60L;
        TimeUnit unit = TimeUnit.SECONDS;
        LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<>(25);

        ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                keepAliveTime, unit, workQueue, new ThreadPoolExecutor.CallerRunsPolicy());

        return executor;
    }
}