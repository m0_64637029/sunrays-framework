package cn.sunxiansheng.web.controller;

import cn.sunxiansheng.tool.response.ResultWrapper;
import cn.sunxiansheng.web.annotation.IgnoredResultWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: 测试
 *
 * @Author sun
 * @Create 2024/10/4 22:56
 * @Version 1.0
 */
@RestController
public class WebController {

    /**
     * 第一种方式：直接使用自动包装成功结果
     *
     * @return
     */
    @RequestMapping("/method1")
    public String method1() {
        return "method1";
    }

    /**
     * 第二种方式：使用 @IgnoredResultWrapper注解忽略掉自动包装
     *
     * @return
     */
    @IgnoredResultWrapper
    @RequestMapping("/method2")
    public String method2() {
        return "method2";
    }

    /**
     * 第三种方式：直接使用ResultWrapper来自己封装结果
     *
     * @return
     */
    @RequestMapping("/method3")
    public ResultWrapper<String> method3() {
        return ResultWrapper.fail("method3");
    }
}