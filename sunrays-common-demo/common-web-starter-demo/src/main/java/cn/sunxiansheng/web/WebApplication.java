package cn.sunxiansheng.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: WebApplication
 *
 * @Author sun
 * @Create 2024/10/4 23:42
 * @Version 1.0
 */
@SpringBootApplication
public class WebApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }
}