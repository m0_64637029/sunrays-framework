package cn.sunxiansheng.env;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: Env
 *
 * @Author sun
 * @Create 2025/1/10 20:53
 * @Version 1.0
 */
@SpringBootApplication
public class EnvApplication {

    public static void main(String[] args) {
        SpringApplication.run(EnvApplication.class, args);
    }
}