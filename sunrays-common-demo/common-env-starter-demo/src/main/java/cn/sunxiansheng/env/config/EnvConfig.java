package cn.sunxiansheng.env.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: Env配置类，测试读取数据
 *
 * @Author sun
 * @Create 2025/1/10 20:55
 * @Version 1.0
 */
@Configuration
@Slf4j
public class EnvConfig {

    @Value("${env.test}")
    private String test;

    @PostConstruct
    public void init() {
        // 测试读取
        log.info("test={}", test);
    }
}