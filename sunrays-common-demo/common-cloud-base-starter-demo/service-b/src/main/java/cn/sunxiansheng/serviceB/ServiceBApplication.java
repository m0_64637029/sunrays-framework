package cn.sunxiansheng.serviceB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: ServiceB 启动类
 *
 * @Author sun
 * @Create 2025/1/8 12:42
 * @Version 1.0
 */
@SpringBootApplication
public class ServiceBApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceBApplication.class, args);
    }
}