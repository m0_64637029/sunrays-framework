package cn.sunxiansheng.serviceB.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: ServiceBController
 *
 * @Author sun
 * @Create 2025/1/8 12:58
 * @Version 1.0
 */
@RestController
public class ServiceBController {

    @RequestMapping("/serviceB")
    public String serviceB() {
        return "serviceB";
    }
}