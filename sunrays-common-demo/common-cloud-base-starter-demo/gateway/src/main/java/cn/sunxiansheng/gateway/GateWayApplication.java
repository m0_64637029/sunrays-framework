package cn.sunxiansheng.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: GateWay 启动类
 *
 * @Author sun
 * @Create 2025/1/8 12:41
 * @Version 1.0
 */
@SpringBootApplication
public class GateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class, args);
    }
}