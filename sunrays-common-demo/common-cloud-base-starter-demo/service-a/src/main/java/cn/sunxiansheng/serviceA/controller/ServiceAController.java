package cn.sunxiansheng.serviceA.controller;

import com.google.gson.Gson;
import cn.sunxiansheng.serviceA.rpc.ServiceBRpc;
import cn.sunxiansheng.tool.response.ResultWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Description: ServiceAController
 *
 * @Author sun
 * @Create 2025/1/8 12:57
 * @Version 1.0
 */
@RestController
public class ServiceAController {

    @RequestMapping("/serviceA")
    public String serviceA() {
        return "serviceA";
    }

    /**
     * 注入rpc接口，一旦调用就会被代理请求
     */
    @Resource
    private ServiceBRpc serviceBRpc;

    @RequestMapping("/rpcToServiceB")
    public String rpcToServiceB() {
        // 获取到的是json
        String json = serviceBRpc.serviceB();
        // 进行手动的json数据转换
        Gson gson = new Gson();
        ResultWrapper resultWrapper = gson.fromJson(json, ResultWrapper.class);
        return (String) resultWrapper.getData();
    }
}