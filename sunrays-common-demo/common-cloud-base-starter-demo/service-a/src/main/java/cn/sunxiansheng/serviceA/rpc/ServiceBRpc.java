package cn.sunxiansheng.serviceA.rpc;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Description: ServiceBRpc
 *
 * @Author sun
 * @Create 2025/1/8 13:18
 * @Version 1.0
 */
@FeignClient(name = "service-b") // 指定要rpc的服务名
public interface ServiceBRpc {

    /**
     * 要请求的方法签名
     *
     * @return
     */
    @RequestMapping("/serviceB")
    String serviceB();
}