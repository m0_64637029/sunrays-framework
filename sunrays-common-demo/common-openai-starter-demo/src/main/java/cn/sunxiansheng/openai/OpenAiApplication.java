package cn.sunxiansheng.openai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: OpenAI启动类
 *
 * @Author sun
 * @Create 2024/12/14 12:23
 * @Version 1.0
 */
@SpringBootApplication
public class OpenAiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenAiApplication.class, args);
    }
}