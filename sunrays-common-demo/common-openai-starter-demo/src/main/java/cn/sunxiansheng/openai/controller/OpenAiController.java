package cn.sunxiansheng.openai.controller;

import cn.sunxiansheng.openai.client.OpenAiClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Description: OpenAI 控制器类
 *
 * @Author sun
 * @Create 2024/12/14 12:27
 * @Version 1.0
 */
@RestController
public class OpenAiController {

    @Resource
    private OpenAiClient openAiClient;

    @RequestMapping("/ask")
    public String ask(String question) {
        String res = openAiClient.askAI("gpt-4o", question, false);
        return "AI回答：" + res;
    }
}