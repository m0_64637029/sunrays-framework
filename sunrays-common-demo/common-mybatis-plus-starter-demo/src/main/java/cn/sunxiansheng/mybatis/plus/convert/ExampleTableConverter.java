package cn.sunxiansheng.mybatis.plus.convert;

import cn.sunxiansheng.mybatis.plus.entity.po.ExampleTable;
import cn.sunxiansheng.mybatis.plus.entity.req.ExampleTableReq;
import cn.sunxiansheng.mybatis.plus.entity.vo.ExampleTableVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 示例表(ExampleTable)转换器：使用MapStruct实现对象之间的转换
 *
 * @Author sun
 * @Create 2025-01-04 17:09:43
 * @Version 1.0
 */
@Mapper
public interface ExampleTableConverter {

    /**
     * 获取 ExampleTableConverter 的代理对象
     */
    ExampleTableConverter INSTANCE = Mappers.getMapper(ExampleTableConverter.class);

    /**
     * 将请求对象转换为实体对象（Req 转换为 Po）
     *
     * @param req 请求对象
     * @return 实体对象
     */
    ExampleTable convertReq2Po(ExampleTableReq req);

    /**
     * 将实体对象转换为视图对象（Po 转换为 Vo）
     *
     * @param po 实体对象
     * @return 视图对象
     */
    ExampleTableVo convertPo2Vo(ExampleTable po);

    /**
     * 将实体对象列表转换为视图对象列表（List Po 转换为 List Vo）
     *
     * @param poList 实体对象列表
     * @return 视图对象列表
     */
    List<ExampleTableVo> convertPoList2VoList(List<ExampleTable> poList);
}
