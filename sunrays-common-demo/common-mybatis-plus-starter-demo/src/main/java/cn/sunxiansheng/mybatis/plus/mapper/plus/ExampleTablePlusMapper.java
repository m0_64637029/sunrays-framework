package cn.sunxiansheng.mybatis.plus.mapper.plus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.sunxiansheng.mybatis.plus.entity.po.ExampleTable;
import org.apache.ibatis.annotations.Mapper;

/**
 * 示例表(ExampleTable)数据库访问层：用来整合MyBatis-Plus
 *
 * @author sun
 * @since 2025-01-04 17:09:43
 */
@Mapper
public interface ExampleTablePlusMapper extends BaseMapper<ExampleTable> {

}
