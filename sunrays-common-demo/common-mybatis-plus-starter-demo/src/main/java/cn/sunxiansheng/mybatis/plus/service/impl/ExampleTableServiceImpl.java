package cn.sunxiansheng.mybatis.plus.service.impl;

import cn.sunxiansheng.mybatis.plus.base.service.impl.SunRaysBaseServiceImpl;
import cn.sunxiansheng.mybatis.plus.convert.ExampleTableConverter;
import cn.sunxiansheng.mybatis.plus.entity.po.ExampleTable;
import cn.sunxiansheng.mybatis.plus.entity.req.ExampleTableReq;
import cn.sunxiansheng.mybatis.plus.entity.vo.ExampleTableVo;
import cn.sunxiansheng.mybatis.plus.mapper.ExampleTableMapper;
import cn.sunxiansheng.mybatis.plus.mapper.plus.ExampleTablePlusMapper;
import cn.sunxiansheng.mybatis.plus.page.PageResult;
import cn.sunxiansheng.mybatis.plus.page.SunPageHelper;
import cn.sunxiansheng.mybatis.plus.service.ExampleTableService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 示例表(ExampleTable)Service实现类：实现业务逻辑
 * 注意：使用this来调用框架自动生成的方法，使用注入的xxxMapper来调用手写的方法
 *
 * @author sun
 * @since 2025-01-04 17:09:43
 */
@Service("exampleTableService")
public class ExampleTableServiceImpl
        extends SunRaysBaseServiceImpl<ExampleTablePlusMapper, ExampleTable, java.lang.Integer>
        implements ExampleTableService {

    // ============================== 注入Mapper ==============================
    @Resource
    private ExampleTableMapper exampleTableMapper;

    // ============================== 框架生成（auto-generation） ==============================

    /**
     * 构造器注入父类的MyBatis-Plus Mapper
     *
     * @param mybatisPlusMapper 父类需要的Plus Mapper
     */
    public ExampleTableServiceImpl(ExampleTablePlusMapper mybatisPlusMapper) {
        super.setMybatisPlusMapper(mybatisPlusMapper);
    }
    // ============================== 框架生成（auto-generation） ==============================

    // ============================== 自定义Service实现 ==============================
    @Override
    @Transactional(rollbackFor = Exception.class) // 开启事务
    public PageResult<ExampleTableVo> listPage(ExampleTableReq exampleTableReq) {
        // req转换为po
        ExampleTable po = ExampleTableConverter.INSTANCE.convertReq2Po(exampleTableReq);

        // 分页查询
        PageResult<ExampleTableVo> paginate = SunPageHelper.paginate(
                exampleTableReq.getPageNo(),
                exampleTableReq.getPageSize(),
                // 调用框架自动生成的方法使用super.xxx ！！！
                () -> super.count(po),
                (offset, limit) -> {
                    // 调用自定义的方法使用注入的xxxMapper.xxx ！！！
                    // 查询数据，并转换为vo
                    List<ExampleTable> exampleTableList =
                            exampleTableMapper.queryPage(po, offset, limit);
                    return ExampleTableConverter.INSTANCE.convertPoList2VoList(exampleTableList);
                }
        );
        return paginate;
    }
}
