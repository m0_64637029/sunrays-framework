package cn.sunxiansheng.mybatis.plus.service;

import cn.sunxiansheng.mybatis.plus.base.service.SunRaysBaseService;
import cn.sunxiansheng.mybatis.plus.entity.po.ExampleTable;
import cn.sunxiansheng.mybatis.plus.entity.req.ExampleTableReq;
import cn.sunxiansheng.mybatis.plus.entity.vo.ExampleTableVo;
import cn.sunxiansheng.mybatis.plus.page.PageResult;

/**
 * 示例表(ExampleTable)Service接口：定义业务逻辑
 *
 * @author sun
 * @since 2025-01-04 17:09:43
 */
public interface ExampleTableService extends SunRaysBaseService<ExampleTable, java.lang.Integer> {

    /**
     * 分页查询
     *
     * @param exampleTableReq 查询条件
     * @return 对象列表
     */
    PageResult<ExampleTableVo> listPage(ExampleTableReq exampleTableReq);
}
