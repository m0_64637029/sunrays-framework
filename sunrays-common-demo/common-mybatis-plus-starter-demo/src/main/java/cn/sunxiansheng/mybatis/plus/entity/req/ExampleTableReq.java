package cn.sunxiansheng.mybatis.plus.entity.req;

import cn.sunxiansheng.mybatis.plus.page.PageInfo;
import lombok.*;

import java.io.Serializable;


/**
 * 示例表(ExampleTable)Req实体类：接受前端请求
 *
 * @author sun
 * @since 2025-01-04 17:17:26
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ExampleTableReq extends PageInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Integer id;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户邮箱
     */
    private String userEmail;
    /**
     * 联系电话
     */
    private String phoneNumber;
    /**
     * 家庭住址
     */
    private String homeAddress;
    /**
     * 账户状态（0-禁用，1-启用）
     */
    private Integer accountStatus;
    /**
     * 逻辑删除标记（0-未删除，1-已删除）
     */
    private Integer isDeleted;
}
