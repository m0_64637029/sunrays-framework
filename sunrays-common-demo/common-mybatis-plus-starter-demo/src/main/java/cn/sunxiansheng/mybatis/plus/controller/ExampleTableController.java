package cn.sunxiansheng.mybatis.plus.controller;

import cn.sunxiansheng.mybatis.plus.entity.po.ExampleTable;
import cn.sunxiansheng.mybatis.plus.entity.req.ExampleTableReq;
import cn.sunxiansheng.mybatis.plus.entity.vo.ExampleTableVo;
import cn.sunxiansheng.mybatis.plus.page.PageResult;
import cn.sunxiansheng.mybatis.plus.service.ExampleTableService;
import cn.sunxiansheng.web.base.BaseController;
import cn.sunxiansheng.tool.response.ResultWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 示例表(ExampleTable)控制层：接收前端请求
 *
 * @author sun
 * @since 2025-01-06 18:56:02
 */
@RestController
@RequestMapping("/example/table")
@Slf4j
public class ExampleTableController extends BaseController {

    // ============================== 注入Service ==============================
    @Resource
    private ExampleTableService exampleTableService;

    // ============================== 基础接口实现 ==============================
    @GetMapping("/existsById/{id}")
    public ResultWrapper<Object> existsById(@PathVariable java.lang.Integer id) {
        return toRes(exampleTableService.existsById(id));
    }

    @GetMapping("/exists")
    public ResultWrapper<Object> exists(@RequestBody ExampleTable po) {
        return toRes(exampleTableService.exists(po));
    }

    @GetMapping("/count")
    public ResultWrapper<Long> count(@RequestBody ExampleTable po) {
        return ResultWrapper.ok(exampleTableService.count(po));
    }

    @GetMapping("/listById/{id}")
    public ResultWrapper<ExampleTable> listById(@PathVariable java.lang.Integer id) {
        return ResultWrapper.ok(exampleTableService.listById(id));
    }

    @GetMapping("/listOne")
    public ResultWrapper<ExampleTable> listOne(@RequestBody ExampleTable po) {
        return ResultWrapper.ok(exampleTableService.listOne(po));
    }

    @GetMapping("/listAll")
    public ResultWrapper<List<ExampleTable>> listAll(@RequestBody ExampleTable po) {
        return ResultWrapper.ok(exampleTableService.listAll(po));
    }

    @PostMapping("/insertOne")
    public ResultWrapper<Object> insertOne(@RequestBody ExampleTable po) {
        return toRes(exampleTableService.insertOne(po));
    }

    @PutMapping("/updateById")
    public ResultWrapper<Object> updateById(@RequestBody ExampleTable po) {
        return toRes(exampleTableService.updateById(po));
    }

    @DeleteMapping("/deleteById/{id}")
    public ResultWrapper<Object> deleteById(@PathVariable java.lang.Integer id) {
        return toRes(exampleTableService.deleteById(id));
    }

    @DeleteMapping("/deleteBatchByIds")
    public ResultWrapper<Object> deleteBatchByIds(@RequestBody List<java.lang.Integer> ids) {
        return toRes(exampleTableService.deleteBatchByIds(ids));
    }

    @DeleteMapping("/delete")
    public ResultWrapper<Object> delete(@RequestBody ExampleTable po) {
        return toRes(exampleTableService.delete(po));
    }

    // ============================== 自定义接口 ==============================

    /**
     * 分页查询数据
     *
     * @param req 筛选条件
     * @return 查询结果
     */
    @GetMapping("/queryPage")
    public ResultWrapper<PageResult<ExampleTableVo>> queryByPage(@RequestBody ExampleTableReq req) {
        // ============================== Preconditions 参数校验 ==============================

        // ============================== 调用Service层 ==============================
        PageResult<ExampleTableVo> pageResult = exampleTableService.listPage(req);
        return ResultWrapper.ok(pageResult);
    }
}
