package cn.sunxiansheng.mybatis.plus.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import cn.sunxiansheng.mybatis.plus.base.entity.BaseEntity;
import lombok.*;

import java.io.Serializable;


/**
 * 示例表(ExampleTable)PO实体类：对应数据库表字段
 *
 * @author sun
 * @since 2025-01-04 17:17:25
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ExampleTable extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键，insert时自动写回id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户邮箱
     */
    private String userEmail;
    /**
     * 联系电话
     */
    private String phoneNumber;
    /**
     * 家庭住址
     */
    private String homeAddress;
    /**
     * 账户状态（0-禁用，1-启用）
     */
    private Integer accountStatus;
    /**
     * 逻辑删除标识(value：未删除，delval：已删除)
     */
    @TableLogic(value = "0", delval = "1")
    private Integer isDeleted;
}
