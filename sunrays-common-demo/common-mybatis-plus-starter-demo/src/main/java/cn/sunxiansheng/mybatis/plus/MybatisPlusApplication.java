package cn.sunxiansheng.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: MybatisPlusApplication
 *
 * @Author sun
 * @Create 2024/10/7 16:36
 * @Version 1.0
 */
@SpringBootApplication
@MapperScan("cn.sunxiansheng.mybatis.plus.mapper")
public class MybatisPlusApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusApplication.class, args);
    }
}