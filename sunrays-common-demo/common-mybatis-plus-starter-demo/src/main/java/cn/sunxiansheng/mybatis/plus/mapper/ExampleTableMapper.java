package cn.sunxiansheng.mybatis.plus.mapper;

import cn.sunxiansheng.mybatis.plus.entity.po.ExampleTable;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 示例表(ExampleTable)数据库访问层
 *
 * @author sun
 * @since 2025-01-04 17:09:43
 */
@Mapper
public interface ExampleTableMapper {

    /**
     * 分页查询
     *
     * @param po     查询条件
     * @param offset 偏移量：计算公式 (pageNo - 1) * pageSize
     * @param limit  页面大小
     * @return 对象列表
     */
    List<ExampleTable> queryPage(@Param("po") ExampleTable po,
                                 @Param("offset") Long offset,
                                 @Param("limit") Long limit);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities 实例对象列表(会封装新增的id)
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<ExampleTable> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities 实例对象列表(会封装新增的id)
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<ExampleTable> entities);
}
