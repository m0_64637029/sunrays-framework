package cn.sunxiansheng.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 邮件服务启动类
 *
 * @Author sun
 * @Create 2025/1/2 17:41
 * @Version 1.0
 */
@SpringBootApplication
public class MailApplication {

    public static void main(String[] args) {
        SpringApplication.run(MailApplication.class, args);
    }
}