package cn.sunxiansheng.mail.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Description: 邮件服务
 *
 * @Author sun
 * @Create 2025/1/2 17:52
 * @Version 1.0
 */
@RestController
public class MailController {

    @Resource
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String from;

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final String to = "1721469477@qq.com";

    /**
     * 发送简单邮件
     */
    @RequestMapping("/sendSimpleMail")
    public void sendSimpleMail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject("使用JavaMailSender和SimpleMailMessage发送邮件");
        message.setText("这是我用Springboot自带的mail启动器给你发送的邮件,当前时间是: " + simpleDateFormat.format(new Date()));
        mailSender.send(message);
    }

    /**
     * 发送HTML邮件
     */
    @RequestMapping("/sendHtmlMessage")
    public void sendHtmlMessage() {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            // 创建邮件发送者地址
            helper.setFrom(new InternetAddress(MimeUtility.encodeText("发送者") + "<" + from + ">"));
            // 创建邮件发送者地址
            helper.setTo(new InternetAddress(MimeUtility.encodeText("接收方") + "<" + to + ">"));
            // 标题
            helper.setSubject("这是一份内容包含HTML标签的网页");
            String content = "<h1>中奖通知</h1>" +
                    "<p style='color:red;'>恭喜你获得大乐透三等奖，点击此处<a>https://www.baidu.com<a>进行领奖</p>";
            // 第二个参数指定发送的是HTML格式
            helper.setText(content, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mailSender.send(message);
    }

    /**
     * 发送带附件的邮件
     */
    @RequestMapping("/sendAttachmentsMail")
    public void sendAttachmentsMail() {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            // 创建邮件发送者地址
            helper.setFrom(new InternetAddress(MimeUtility.encodeText("发送者") + "<" + from + ">"));
            // 创建邮件发送者地址
            helper.setTo(new InternetAddress(MimeUtility.encodeText("接收方") + "<" + to + ">"));
            helper.setSubject("报销申请-报销明细在附件里");
            String content = "<h1>报销单</h1>" +
                    "<p style='color:red;'>报销详情请下载附件进行查看</p>";
            // 第二个参数指定发送的是HTML格式
            helper.setText(content, true);
            // 添加附件
            helper.addAttachment("报销费用.png", new FileSystemResource(new File("/Users/sunxiansheng/Pictures/image.png")));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mailSender.send(message);
    }

    /**
     * 发送带静态资源的邮件
     */
    @RequestMapping("/sendInlineMail")
    public void sendInlineMail() {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            // 创建邮件发送者地址
            helper.setFrom(new InternetAddress(MimeUtility.encodeText("发送者") + "<" + from + ">"));
            // 创建邮件发送者地址
            helper.setTo(new InternetAddress(MimeUtility.encodeText("接收方") + "<" + to + ">"));
            helper.setSubject("邮件里包含静态资源，请注意查收");
            //第二个参数指定发送的是HTML格式
            helper.setText("<html><body>带静态资源的邮件内容 图片:<img src='cid:fee'/></body></html>", true);

            FileSystemResource file = new FileSystemResource(new File("/Users/sunxiansheng/Pictures/image.png"));
            helper.addInline("fee", file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mailSender.send(message);
    }
}