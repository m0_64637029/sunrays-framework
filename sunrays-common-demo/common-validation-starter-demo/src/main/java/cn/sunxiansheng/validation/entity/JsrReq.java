package cn.sunxiansheng.validation.entity;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Description: JSR-303校验请求参数
 *
 * @Author sun
 * @Create 2024/10/28 11:16
 * @Version 1.0
 */
@Data
public class JsrReq {

    // 基础校验
    @NotNull(message = "ID不能为空")
    private Long id;

    @NotBlank(message = "用户名不能为空")
    @Size(min = 3, max = 20, message = "用户名长度必须在3到20个字符之间")
    private String username;

    @NotBlank(message = "电子邮件不能为空")
    @Email(message = "电子邮件格式不正确")
    private String email;

    // 数值校验
    @NotNull(message = "年龄不能为空")
    @Min(value = 18, message = "年龄不能小于18岁")
    @Max(value = 60, message = "年龄不能大于60岁")
    private Integer age;

    @NotNull(message = "薪资不能为空")
    @DecimalMin(value = "5000.00", message = "薪资不能低于5000.00")
    @DecimalMax(value = "100000.00", message = "薪资不能高于100000.00")
    private BigDecimal salary;

    // 日期校验
    @NotNull(message = "生日不能为空")
    @Past(message = "生日必须是过去的日期")
    private Date birthDate;

    @NotNull(message = "注册日期不能为空")
    @PastOrPresent(message = "注册日期必须是过去或当前日期")
    private Date registrationDate;

    @NotNull(message = "有效期不能为空")
    @FutureOrPresent(message = "有效期必须是未来或当前日期")
    private Date expirationDate;

    // 布尔校验
    @AssertTrue(message = "必须接受条款")
    private Boolean acceptedTerms;

    @AssertFalse(message = "用户不能被封禁")
    private Boolean isBanned;

    // 字符串和正则表达式校验
    @NotBlank(message = "密码不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9]{6,12}$", message = "密码必须是6到12位的字母或数字")
    private String password;

    // 集合校验
    @NotEmpty(message = "电话号码列表不能为空")
    private List<@Pattern(regexp = "^\\+?[0-9]{10,15}$", message = "电话号码格式不正确") String> phoneNumbers;

    @NotEmpty(message = "技能标签不能为空")
    @Size(min = 1, max = 5, message = "技能标签数量必须在1到5之间")
    private List<@Size(min = 1, max = 20, message = "每个技能标签长度必须在1到20个字符之间") String> skillTags;

    // 嵌套对象校验
    @NotEmpty(message = "地址列表不能为空")
    @Valid
    private List<@Valid Address> addresses;

    // 嵌套类：地址信息
    @Data
    public static class Address {
        @NotBlank(message = "街道不能为空")
        private String street;

        @NotBlank(message = "城市不能为空")
        private String city;

        @NotBlank(message = "邮政编码不能为空")
        @Pattern(regexp = "^[0-9]{5}$", message = "邮政编码必须是5位数字")
        private String postalCode;
    }
}