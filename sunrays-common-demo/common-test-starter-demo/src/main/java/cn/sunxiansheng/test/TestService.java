package cn.sunxiansheng.test;

import org.springframework.stereotype.Service;

/**
 * Description: 测试服务类
 *
 * @Author sun
 * @Create 2024/11/12 15:37
 * @Version 1.0
 */
@Service
public class TestService {

    public String test() {
        return "test";
    }
}