package cn.sunxiansheng.test;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * Description:
 *
 * @Author sun
 * @Create 2024/11/12 15:38
 * @Version 1.0
 */
@SpringBootTest
class TestServiceTest {

    @Resource
    private TestService testService;

    @Test
    void test1() {
        String test = testService.test();
        System.out.println("test = " + test);
    }
}