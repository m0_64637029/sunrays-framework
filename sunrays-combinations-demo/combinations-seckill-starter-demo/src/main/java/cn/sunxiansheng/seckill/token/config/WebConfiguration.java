package cn.sunxiansheng.seckill.token.config;

import cn.sunxiansheng.seckill.token.filter.TokenFilter;
import cn.sunxiansheng.redis.utils.RString;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * Description: WebMvcConfigurer
 *
 * @Author sun
 * @Create 2025/1/8 16:14
 * @Version 1.0
 */
@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Bean
    public FilterRegistrationBean<TokenFilter> tokenFilter() {
        FilterRegistrationBean<TokenFilter> registrationBean = new FilterRegistrationBean<>();

        registrationBean.setFilter(new TokenFilter(new RString(redisTemplate)));
        // 只对 /buyApple的请求进行Token校验
        registrationBean.addUrlPatterns("/buyApple");
        registrationBean.setOrder(10);

        return registrationBean;
    }
}