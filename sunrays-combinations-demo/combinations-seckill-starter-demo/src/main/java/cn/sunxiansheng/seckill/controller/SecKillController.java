package cn.sunxiansheng.seckill.controller;

import cn.sunxiansheng.seckill.token.filter.TokenFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: SecKillController
 *
 * @Author sun
 * @Create 2025/1/8 15:41
 * @Version 1.0
 */
@RestController
@Slf4j
public class SecKillController {

    @RequestMapping("/test")
    public String test() {
        return "test";
    }

    /**
     * 模拟买苹果的操作（秒杀）
     *
     * @return
     */
    @RequestMapping("/buyApple")
    public String buyApple() {
        // 获取到Token，作为下单这个操作的幂等号，可以在创建订单时存到订单的幂等号字段
        String token = TokenFilter.TOKEN_THREAD_LOCAL.get();
        log.info("下单的幂等号:{}", token);
        // 下单的前置校验：库存够不够
        return "buyApple";
    }
}