package cn.sunxiansheng.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 秒杀 启动类
 *
 * @Author sun
 * @Create 2025/1/8 15:40
 * @Version 1.0
 */
@SpringBootApplication
public class SecKillApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecKillApplication.class, args);
    }
}