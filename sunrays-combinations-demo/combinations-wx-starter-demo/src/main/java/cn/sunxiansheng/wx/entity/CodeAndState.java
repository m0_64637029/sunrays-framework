package cn.sunxiansheng.wx.entity;

import lombok.Data;

/**
 * Description: 接受code和state的bean
 *
 * @Author sun
 * @Create 2025/1/16 19:15
 * @Version 1.0
 */

@Data
public class CodeAndState {
    /**
     * 微信的code
     */
    private String code;
    /**
     * 微信的state
     */
    private String state;
}