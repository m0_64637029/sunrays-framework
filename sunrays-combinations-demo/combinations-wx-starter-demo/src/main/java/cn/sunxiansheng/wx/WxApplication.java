package cn.sunxiansheng.wx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 微信启动类
 *
 * @Author sun
 * @Create 2025/1/13 17:54
 * @Version 1.0
 */
@SpringBootApplication
public class WxApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxApplication.class, args);
    }
}