package cn.sunxiansheng.wx.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

/**
 * Description: 通过code换取的access_token
 *
 * @Author sun
 * @Create 2025/1/16 19:17
 * @Version 1.0
 */
@Data
public class AccessTokenResponse {

    @SerializedName("access_token")
    private String accessToken;

    @SerializedName("expires_in")
    private Integer expiresIn;

    @SerializedName("refresh_token")
    private String refreshToken;

    @SerializedName("openid")
    private String openid;

    @SerializedName("scope")
    private String scope;

    @SerializedName("unionid")
    private String unionid;
}