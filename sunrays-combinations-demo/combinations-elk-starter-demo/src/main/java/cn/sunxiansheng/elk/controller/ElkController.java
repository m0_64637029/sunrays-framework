package cn.sunxiansheng.elk.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: 测试
 *
 * @Author sun
 * @Create 2025/1/1 13:19
 * @Version 1.0
 */
@RestController
public class ElkController {

    @RequestMapping("/test")
    public void test() {
        throw new RuntimeException("test");
    }
}