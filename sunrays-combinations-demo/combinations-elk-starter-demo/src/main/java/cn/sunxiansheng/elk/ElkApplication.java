package cn.sunxiansheng.elk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: ELK日志收集系统
 *
 * @Author sun
 * @Create 2024/12/28 13:59
 * @Version 1.0
 */
@SpringBootApplication
public class ElkApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElkApplication.class, args);
    }
}