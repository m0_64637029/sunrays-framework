package cn.sunxiansheng.intelligent.analysis.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description:  RabbitMQ配置类
 *
 * @Author sun
 * @Create 2025/1/1 17:09
 * @Version 1.0
 */
@Configuration
public class RabbitMQConfig {

    /**
     * 死信交换机
     *
     * @return
     */
    @Bean
    public DirectExchange dlxExchange() {
        return new DirectExchange("dlxExchange");
    }

    /**
     * 死信队列
     *
     * @return
     */
    @Bean
    public Queue dlxQueue() {
        return QueueBuilder.durable("dlxQueue")
                .build();
    }

    /**
     * 死信队列绑定死信交换机
     *
     * @return
     */
    @Bean
    public Binding dlxBinding() {
        return BindingBuilder
                .bind(dlxQueue())
                .to(dlxExchange())
                .with("dlx.elk");
    }

    /**
     * 创建一个fanout类型的elk交换机
     *
     * @return
     */
    @Bean
    public FanoutExchange elkExchange() {
        return new FanoutExchange("elk.exchange");
    }

    /**
     * 创建一个elk队列，并设置死信交换机和死信路由键
     *
     * @return
     */
    @Bean
    public Queue elkQueue() {
        return QueueBuilder.durable("elkQueue")
                .withArgument("x-dead-letter-exchange", "dlxExchange")
                .withArgument("x-dead-letter-routing-key", "dlx.elk")
                .lazy()
                .build();
    }

    /**
     * 交换机和队列绑定
     */
    @Bean
    public Binding binding() {
        return BindingBuilder.bind(elkQueue()).to(elkExchange());
    }
}