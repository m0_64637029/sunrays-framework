package cn.sunxiansheng.intelligent.analysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: 智能分析启动类
 *
 * @Author sun
 * @Create 2025/1/1 19:29
 * @Version 1.0
 */
@SpringBootApplication
public class IntelligentAnalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntelligentAnalysisApplication.class, args);
    }
}