package cn.sunxiansheng.intelligent.analysis.utils;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Description: 字符串工具类
 *
 * @Author sun
 * @Create 2025/1/2 15:26
 * @Version 1.0
 */
public class StringUtils {

    /**
     * 截取字符串的前n个字符。
     * 如果字符串长度小于n，则返回原字符串。
     *
     * @param input 原字符串
     * @param n     截取的字符数
     * @return 截取后的字符串
     */
    public static String truncate(String input, int n) {
        if (input == null || n <= 0) {
            return "";
        }
        return input.length() > n ? input.substring(0, n) : input;
    }

    /**
     * 按字节数截取字符串（支持多字节字符）。
     * 如果字符串总字节数小于限制，直接返回原字符串。
     *
     * @param input     原字符串
     * @param byteLimit 最大字节数
     * @return 截取后的字符串
     */
    public static String truncateByBytes(String input, int byteLimit) {
        if (input == null || byteLimit <= 0) {
            return "";
        }
        byte[] bytes = input.getBytes(StandardCharsets.UTF_8);
        if (bytes.length <= byteLimit) {
            return input;
        }

        // 按字节截取字符串
        int endIndex = 0;
        int currentBytes = 0;
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            // UTF-8编码：ASCII占1字节，其他占2或3字节
            currentBytes += (c <= 0x7F) ? 1 : (c <= 0x7FF ? 2 : 3);
            if (currentBytes > byteLimit) {
                break;
            }
            endIndex = i + 1;
        }
        return input.substring(0, endIndex);
    }

    /**
     * 截取字符串的前n个字符并在超长时添加省略号（...）。
     *
     * @param input 原字符串
     * @param n     截取的字符数
     * @return 截取后的字符串（可能包含省略号）
     */
    public static String truncateWithEllipsis(String input, int n) {
        if (input == null || n <= 0) {
            return "";
        }
        if (input.length() <= n) {
            return input;
        }
        return input.substring(0, n) + "...";
    }

    /**
     * 按字节截取字符串并添加省略号（...）。
     *
     * @param input     原字符串
     * @param byteLimit 最大字节数
     * @return 截取后的字符串（可能包含省略号）
     */
    public static String truncateByBytesWithEllipsis(String input, int byteLimit) {
        if (input == null || byteLimit <= 0) {
            return "";
        }
        String truncated = truncateByBytes(input, byteLimit - 3);
        return truncated.length() < input.length() ? truncated + "..." : truncated;
    }

    /**
     * 合并字符串列表，截取指定长度，并在超长时添加省略号。
     *
     * @param stringList 字符串列表
     * @param maxLength  最大字符数
     * @return 截取后的字符串（可能包含省略号）
     */
    public static String mergeAndTruncateWithEllipsis(List<String> stringList, int maxLength) {
        if (stringList == null || stringList.isEmpty() || maxLength <= 0) {
            return "";
        }

        // 合并字符串
        StringBuilder merged = new StringBuilder();
        for (String str : stringList) {
            if (str != null) {
                merged.append(str);
            }
        }

        // 截取字符串
        String result = merged.toString();
        if (result.length() > maxLength) {
            return result.substring(0, maxLength) + "...";
        }
        return result;
    }
}