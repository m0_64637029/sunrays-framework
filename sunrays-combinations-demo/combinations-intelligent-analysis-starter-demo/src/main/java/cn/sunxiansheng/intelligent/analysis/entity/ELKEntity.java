package cn.sunxiansheng.intelligent.analysis.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description: ELK实体类
 *
 * @Author sun
 * @Create 2025/1/1 18:08
 * @Version 1.0
 */
@Data
public class ELKEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String traceId;

    private String thread;

    private String logger;

    private String throwable;

    private String module;

    private String level;

    private String timestamp;

    private String host;

    @JsonProperty("log_message")
    private String logMessage;
}