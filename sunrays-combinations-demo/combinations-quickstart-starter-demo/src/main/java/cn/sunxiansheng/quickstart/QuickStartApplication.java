package cn.sunxiansheng.quickstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: QuickStartApplication
 *
 * @Author sun
 * @Create 2025/1/17 19:32
 * @Version 1.0
 */
@SpringBootApplication
public class QuickStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuickStartApplication.class, args);
    }
}
