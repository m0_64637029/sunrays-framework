package cn.sunxiansheng.quickstart.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: QuickStartController
 *
 * @Author sun
 * @Create 2025/1/18 13:52
 * @Version 1.0
 */
@RestController
public class QuickStartController {

    /**
     * A test endpoint.
     *
     * @return A sample response.
     */
    @RequestMapping("/test")
    public String test() {
        return "This is a test response from QuickStartController";
    }
}
