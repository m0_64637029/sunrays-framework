# README

## 1.概述

**Sunrays-Framework** 是一款基于 **Spring Boot** 构建的高效微服务开发框架，深度融合了 **Spring Cloud** 生态中的核心技术组件，涵盖了以下关键功能：

- **MyBatis-Plus**：简化数据访问层的开发。
- **Minio**：提供稳定、高效的分布式文件存储支持。
- **Redis**：实现缓存、分布式锁等高性能存储功能。
- **RabbitMQ**：可靠的消息队列支持，适用于异步任务和消息通知。
- **Log4j2**：提供灵活、性能卓越的日志管理。
- **Nacos**：负责服务发现与配置管理，确保系统动态可扩展。
- **Spring Cloud Gateway**：高性能的 API 网关，支持路由与负载均衡。
- **OpenFeign**：声明式 HTTP 客户端，简化服务间通信。
- **OpenAI**：为智能化应用提供接入支持。
- **Mail**：内置邮件服务功能，支持多场景通知需求。
- **微信支付与登录**：完整集成微信支付功能和微信授权登录，提升用户体验。

框架注重 **高效性、可扩展性和易维护性**，为开发者提供开箱即用的解决方案，极大地简化了微服务架构的搭建过程。无论是构建企业级分布式系统还是完成毕设项目，**Sunrays-Framework** 都能以其强大的模块化设计与全面的技术支持，帮助开发者快速实现目标、专注于业务逻辑的创新与优化。

---

### 1.**主要功能**

![CleanShot 2025-01-20 at 12.14.57@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-20%20at%2012.14.57%402x.png)

---

> **本人在此承诺：只要我做Java一天，框架就会不断的升级！**
>
> **永久开源，永久免费！**

---

### 2.**相关链接**

- **CSDN**：[Sunrays-Framework 开发笔记](https://blog.csdn.net/m0_64637029/category_12873090.html?spm=1001.2014.3001.5482)
- **官方文档**：[Sunrays-Framework 官方文档](http://sunrays.wiki/)  **必读部分提供了七万字的完整语雀开发文档**

![CleanShot 2025-01-20 at 11.42.31@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-20%20at%2011.42.31%402x.png)

- 微信交流群

![CleanShot 2025-01-20 at 18.49.34@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-20%20at%2018.49.34%402x.png)



## 2.系统要求

为确保系统正常运行，以下是本项目所需的技术栈及版本要求。

---

### 构建工具

| 工具      | 版本             |
| --------- | ---------------- |
| **Maven** | 3.6.3 或更高版本 |

---

### 框架和语言

| 技术                     | 版本     |
| ------------------------ | -------- |
| **Spring Boot**          | 2.4.2    |
| **Spring Cloud**         | 2020.0.1 |
| **Spring Cloud Alibaba** | 2021.1   |
| **JDK**                  | 1.8      |

---

### 数据库与缓存

| 技术      | 版本  |
| --------- | ----- |
| **MySQL** | 5.7   |
| **Redis** | 6.2.6 |

---

### 消息队列与对象存储

| 技术         | 版本       |
| ------------ | ---------- |
| **RabbitMQ** | 3.8.8      |
| **MinIO**    | 2024-12-19 |

## 3.快速入门

### 0.配置Maven中央仓库

#### 1.打开settings.xml

![CleanShot 2025-01-19 at 23.16.59@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-19%20at%2023.16.59%402x-20250120124158580.png)

#### 2.不要配置阿里云，切换为Maven中央仓库，否则下不了依赖

```xml
  <mirrors>
    <mirror>
      <id>central</id>
      <mirrorOf>central</mirrorOf> <!-- 直接指定中央仓库 -->
      <name>Maven Central</name>
      <url>https://repo.maven.apache.org/maven2</url>
    </mirror>
  </mirrors>
```

### 1.创建项目 combinations-quickstart-starter-demo

![CleanShot 2025-01-18 at 18.50.39@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-18%20at%2018.50.39%402x-20250120124158740.png)

### 2.基本目录结构

![CleanShot 2025-01-18 at 23.05.33@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-18%20at%2023.05.33%402x-20250120124159226.png)

### 3.代码

#### 1.pom.xml 引入quickstart依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>cn.sunxiansheng</groupId>
        <artifactId>sunrays-framework-demo</artifactId>
        <version>1.0-SNAPSHOT</version>
    </parent>

    <artifactId>combinations-quickstart-starter-demo</artifactId>

    <!-- 通过properties来指定版本号 -->
    <properties>
        <!-- 指定编译版本 -->
        <java.version>1.8</java.version>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <!-- 指定Sunrays-Framework的版本 -->
        <sunrays.version>1.0.0</sunrays.version>
    </properties>

    <dependencyManagement>
        <!-- 使用sunrays-dependencies来管理依赖，则依赖无需加版本号 -->
        <dependencies>
            <dependency>
                <groupId>cn.sunxiansheng</groupId>
                <artifactId>sunrays-dependencies</artifactId>
                <version>${sunrays.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- 快速入门的一个starter -->
        <dependency>
            <groupId>cn.sunxiansheng</groupId>
            <artifactId>combinations-quickstart-starter</artifactId>
            <!-- 无需指定版本 -->
        </dependency>
    </dependencies>
</project>
```

#### 2.application.yml 配置日志存储根目录

```yml
sun-rays:
  log4j2:
    home: /Users/sunxiansheng/IdeaProjects/sunrays-framework-demo/combinations-quickstart-starter-demo/logs # 日志存储的根目录
```

#### 3.QuickStartController.java 测试的Controller

```java
package cn.sunxiansheng.quickstart.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: QuickStartController
 *
 * @Author sun
 * @Create 2025/1/18 19:17
 * @Version 1.0
 */
@RestController
public class QuickStartController {

    /**
     * A test endpoint.
     *
     * @return A sample response.
     */
    @RequestMapping("/test")
    public String test() {
        return "This is a test response from QuickStartController";
    }
}
```

#### 4.QuickStartApplication.java 启动类

```java
package cn.sunxiansheng.quickstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Description: QuickStartApplication
 *
 * @Author sun
 * @Create 2025/1/18 18:52
 * @Version 1.0
 */
@SpringBootApplication
public class QuickStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuickStartApplication.class, args);
    }
}
```

#### 5.测试运行

![CleanShot 2025-01-18 at 23.00.01@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-18%20at%2023.00.01%402x-20250120124201200.png)

#### 6.请求测试

##### 1.Controller的返回结果是String，默认自动被包装了

![CleanShot 2025-01-18 at 22.50.50@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-18%20at%2022.50.50%402x-20250120124202947.png)

##### 2.自带链路追踪以及全链路的日志输出

![CleanShot 2025-01-18 at 22.49.06@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-18%20at%2022.49.06%402x-20250120124203630.png)

## 4.基础知识（必读）

### 1.项目架构

#### 1.图示

![CleanShot 2025-01-18 at 23.14.51@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-18%20at%2023.14.51%402x-20250120124206058.png)

#### 2.说明

##### 1. `sunrays-dependencies`

- **职责**：管理项目的依赖版本及配置。
- **描述**：独立模块，不继承其他模块。

##### 2. `sunrays-framework`

- **职责**：统一管理整个项目，继承 `sunrays-dependencies`。
- **描述**：作为项目的核心框架模块，负责整体项目的基础配置和管理。

##### 3. `sunrays-common`

- **职责**：封装通用组件，继承 `sunrays-framework`。
- **描述**：包括一些通用的工具类、公共模块等。

##### 4. `sunrays-common-cloud`

- **职责**：封装 Cloud 相关的通用组件，继承 `sunrays-framework`。
- **描述**：包括 Cloud 相关的基础设施和服务封装。

##### 5. `sunrays-common-demo`

- **职责**：提供 `sunrays-common` 和 `sunrays-common-cloud` 的测试 demo，继承 `sunrays-framework`。
- **描述**：用于验证 `sunrays-common` 和 `sunrays-common-cloud` 子模块的功能和集成。

##### 6. `sunrays-combinations`

- **职责**：管理业务依赖，或者作为中台，继承 `sunrays-framework`。
- **描述**：通过组合 `sunrays-common` 和 `sunrays-common-cloud` 完成具体的业务依赖管理。

##### 7. `sunrays-combinations-demo`

- **职责**：提供 `sunrays-combinations` 模块的测试 demo，继承 `sunrays-framework`。
- **描述**：用于验证 `sunrays-combinations` 模块的功能和集成。

### 2.common-log4j2-starter说明

#### 1.这个模块是必须被引入的！

#### 2.对于sunrays-combinations

如果引入了sunrays-combinations模块的依赖，就不需要额外引入common-log4j2-starter，因为已经默认包含了。

#### 3.对于sunrays-common或者sunrays-common-cloud

如果引入的是sunrays-common或者sunrays-common-cloud，那么就需要额外引入common-log4j2-starter。

### 3.隐私数据保护的问题

#### 1.引入common-env-starter

这个依赖一旦引入，就可以在application.yml配置文件中配置.env文件的路径，然后通过$占位符来从.env文件中读取隐私数据，我在后面的介绍中都会采用这种方式。

#### 2.不引入common-env-starter

在实际开发中，如果不需要进行隐私数据的保护，就可以不引入这个依赖：

1. 不需要在application.yml配置文件中配置.env文件的路径，也不需要创建.env文件了
2. 将我示例中的${xxx}的部分直接替换为真实的数据即可，比如：

![CleanShot 2025-01-19 at 12.48.42@2x](https://mac-picture.oss-cn-beijing.aliyuncs.com/CleanShot%202025-01-19%20at%2012.48.42%402x-20250120124206335.png)