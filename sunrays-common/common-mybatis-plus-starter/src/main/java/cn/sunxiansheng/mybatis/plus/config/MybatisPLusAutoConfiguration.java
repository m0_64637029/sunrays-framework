package cn.sunxiansheng.mybatis.plus.config;

import cn.sunxiansheng.mybatis.plus.config.properties.MyBatisPlusProperties;
import cn.sunxiansheng.mybatis.plus.interceptor.SqlBeautyInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: MybatisPLus自动配置类
 *
 * @Author sun
 * @Create 2024/10/23 23:00
 * @Version 1.0
 */
@Configuration
@EnableConfigurationProperties({MyBatisPlusProperties.class}) // 启用配置类
@Slf4j
public class MybatisPLusAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("MybatisPLusAutoConfiguration has been loaded successfully!");
    }

    /**
     * SQL美化拦截器
     */
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnProperty(prefix = "sun-rays.mybatis-plus", value = "sql-beauty-enabled", havingValue = "true", matchIfMissing = true)
    public SqlBeautyInterceptor sqlBeautyInterceptor() {
        log.info("SqlBeautyInterceptor 成功注入!");
        return new SqlBeautyInterceptor();
    }
}