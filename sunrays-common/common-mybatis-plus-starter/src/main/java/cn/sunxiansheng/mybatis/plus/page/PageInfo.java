package cn.sunxiansheng.mybatis.plus.page;

import lombok.*;

import java.io.Serializable;

/**
 * Description: 分页请求的基础参数
 *
 * @Author sun
 * @Create 2024-10-23 22:27:32
 * @Version 1.1
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PageInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 页码，默认为1
     */
    private Long pageNo = 1L;

    /**
     * 每页显示的记录数，默认为20
     */
    private Long pageSize = 20L;

    public Long getPageNo() {
        return (pageNo == null || pageNo < 1) ? 1 : pageNo;
    }

    public Long getPageSize() {
        return (pageSize == null || pageSize < 1) ? 20 : pageSize;
    }

    public PageInfo setPageNo(Long pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public PageInfo setPageSize(Long pageSize) {
        this.pageSize = pageSize;
        return this;
    }
}
