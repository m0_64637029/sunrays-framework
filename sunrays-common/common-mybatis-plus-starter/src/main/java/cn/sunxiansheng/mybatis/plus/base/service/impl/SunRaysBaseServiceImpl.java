package cn.sunxiansheng.mybatis.plus.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.sunxiansheng.mybatis.plus.base.service.SunRaysBaseService;

import java.io.Serializable;
import java.util.List;

/**
 * Description: SunRays-Framework 基础Service实现类
 *
 * @Author sun
 * @Create 2024/10/25 17:38
 * @Version 1.0
 */
public class SunRaysBaseServiceImpl<M extends BaseMapper<T>, T, ID extends Serializable>
        implements SunRaysBaseService<T, ID> {

    // Mapper字段，私有化处理，不允许子类直接访问，只能通过Setter注入
    private M mybatisPlusMapper;

    // Setter方法用于注入MyBatis-Plus Mapper
    public void setMybatisPlusMapper(M mybatisPlusMapper) {
        this.mybatisPlusMapper = mybatisPlusMapper;
    }

    // ============================== ,CRUD方法 ==============================

    @Override
    public boolean existsById(ID id) {
        T t = mybatisPlusMapper.selectById(id);
        return t != null;
    }

    @Override
    public boolean exists(T po) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(po);
        return mybatisPlusMapper.exists(queryWrapper);
    }

    @Override
    public Long count(T po) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(po);
        return mybatisPlusMapper.selectCount(queryWrapper);
    }

    @Override
    public T listById(ID id) {
        return mybatisPlusMapper.selectById(id);
    }

    @Override
    public T listOne(T po) {
        return mybatisPlusMapper.selectOne(new QueryWrapper<>(po));
    }

    @Override
    public List<T> listAll(T po) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(po);
        return mybatisPlusMapper.selectList(queryWrapper);
    }

    @Override
    public int insertOne(T po) {
        return mybatisPlusMapper.insert(po);
    }

    @Override
    public int updateById(T po) {
        return mybatisPlusMapper.updateById(po);
    }

    @Override
    public int update(T po, T condition) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(condition);
        return mybatisPlusMapper.update(po, queryWrapper);
    }

    @Override
    public int deleteById(ID id) {
        return mybatisPlusMapper.deleteById(id);
    }

    @Override
    public int deleteBatchByIds(List<ID> ids) {
        return mybatisPlusMapper.deleteBatchIds(ids);
    }

    @Override
    public int delete(T po) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(po);
        return mybatisPlusMapper.delete(queryWrapper);
    }
}