package cn.sunxiansheng.mybatis.plus.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Description: Entity基类
 *
 * @Author sun
 * @Create 2024/10/26 10:35
 * @Version 1.0
 */
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建者
     */
    @TableField("create_by")  // 数据库中的字段名
    private String createBy;

    /**
     * 创建时间
     */
    @TableField("create_time")  // 数据库中的字段名
    private Date createTime;

    /**
     * 更新者
     */
    @TableField("update_by")  // 数据库中的字段名
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField("update_time")  // 数据库中的字段名
    private Date updateTime;
}