package cn.sunxiansheng.mybatis.plus.base.service;

import java.io.Serializable;
import java.util.List;

/**
 * Description: SunRays-Framework 基础Service接口
 *
 * @Author sun
 * @Create 2024/10/25 16:21
 * @Version 1.0
 */
public interface SunRaysBaseService<T, ID extends Serializable> {

    /**
     * 根据主键判断数据是否存在
     *
     * @param id 主键
     * @return 是否存在
     */
    boolean existsById(ID id);

    /**
     * 根据条件判断数据是否存在
     *
     * @param po
     * @return
     */
    boolean exists(T po);

    /**
     * 根据条件统计行数
     *
     * @param po 查询条件
     * @return 总行数
     */
    Long count(T po);

    /**
     * 根据主键查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    T listById(ID id);

    /**
     * 根据条件查询单条数据
     *
     * @param po
     * @return
     */
    T listOne(T po);

    /**
     * 根据条件查询记录
     *
     * @param po 查询条件
     * @return 对象列表
     */
    List<T> listAll(T po);

    /**
     * 新增数据
     *
     * @param po 实例对象（如果设置了@TableId注解，会自动回写主键）
     * @return 影响行数
     */
    int insertOne(T po);

    /**
     * 根据主键修改数据
     *
     * @param po 实例对象
     * @return 影响行数
     */
    int updateById(T po);

    /**
     * 根据条件修改数据
     *
     * @param po
     * @param condition
     * @return
     */
    int update(T po, T condition);

    /**
     * 根据主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(ID id);

    /**
     * 根据主键批量删除数据
     *
     * @param ids 主键列表
     * @return 影响行数
     */
    int deleteBatchByIds(List<ID> ids);

    /**
     * 根据条件删除数据
     *
     * @param po
     * @return
     */
    int delete(T po);
}