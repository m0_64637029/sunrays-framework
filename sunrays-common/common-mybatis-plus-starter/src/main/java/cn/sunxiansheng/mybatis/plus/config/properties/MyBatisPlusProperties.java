package cn.sunxiansheng.mybatis.plus.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: MyBatisPlus配置
 *
 * @Author sun
 * @Create 2025/1/3 21:36
 * @Version 1.0
 */
@ConfigurationProperties(prefix = "sun-rays.mybatis-plus")
@Data
public class MyBatisPlusProperties {

    /**
     * 是否启用sql美化
     */
    private boolean sqlBeautyEnabled = true;
}