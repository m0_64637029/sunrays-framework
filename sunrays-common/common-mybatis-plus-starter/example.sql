CREATE TABLE example_table
(
    id             INT PRIMARY KEY COMMENT '主键ID',
    user_name      VARCHAR(255) NULL COMMENT '用户名称',
    user_email     VARCHAR(255) NULL COMMENT '用户邮箱',
    phone_number   VARCHAR(20)  DEFAULT NULL COMMENT '联系电话',
    home_address   VARCHAR(255) DEFAULT NULL COMMENT '家庭住址',
    account_status TINYINT(1)   DEFAULT 0 COMMENT '账户状态（0-禁用，1-启用）',
    create_by      VARCHAR(50)  DEFAULT NULL COMMENT '创建人',
    create_time    DATETIME COMMENT '创建时间',
    update_by      VARCHAR(50)  DEFAULT NULL COMMENT '更新人',
    update_time    DATETIME COMMENT '更新时间',
    is_deleted     TINYINT(1)   DEFAULT 0 COMMENT '逻辑删除标记（0-未删除，1-已删除）'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='示例表';

INSERT INTO example_table (id, user_name, user_email, phone_number, home_address, account_status, create_by)
VALUES (1, '张三', 'zhangsan@example.com', '13800138000', '北京市朝阳区', 1, 'admin'),
       (2, '李四', 'lisi@example.com', '13800138001', '上海市浦东新区', 1, 'admin'),
       (3, '王五', 'wangwu@example.com', '13800138002', '广州市天河区', 0, 'admin'),
       (4, '赵六', 'zhaoliu@example.com', '13800138003', '深圳市福田区', 1, 'admin'),
       (5, '孙七', 'sunqi@example.com', '13800138004', '成都市武侯区', 0, 'admin'),
       (6, '周八', 'zhouba@example.com', '13800138005', '杭州市西湖区', 1, 'admin'),
       (7, '吴九', 'wujia@example.com', '13800138006', '重庆市渝中区', 0, 'admin'),
       (8, '郑十', 'zhengshi@example.com', '13800138007', '南京市鼓楼区', 1, 'admin'),
       (9, '冯十一', 'fengshiyi@example.com', '13800138008', '武汉市武昌区', 1, 'admin'),
       (10, '褚十二', 'chushier@example.com', '13800138009', '长沙市岳麓区', 0, 'admin');
