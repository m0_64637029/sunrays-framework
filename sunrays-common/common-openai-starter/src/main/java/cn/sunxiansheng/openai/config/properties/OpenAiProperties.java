package cn.sunxiansheng.openai.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: OpenAI配置属性类
 *
 * @Author sun
 * @Create 2024/12/14 11:44
 * @Version 1.0
 */
@Data
@ConfigurationProperties(prefix = "sun-rays.openai")
public class OpenAiProperties {

    /**
     * OpenAI API Key
     */
    private String apiKey;

    /**
     * OpenAI API URL 有默认值
     */
    private String apiUrl = "https://api.openai.com/v1/chat/completions";
}