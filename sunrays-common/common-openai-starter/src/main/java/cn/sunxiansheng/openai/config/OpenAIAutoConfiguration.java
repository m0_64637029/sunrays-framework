package cn.sunxiansheng.openai.config;

import cn.sunxiansheng.openai.client.OpenAiClient;
import cn.sunxiansheng.openai.config.properties.OpenAiProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: OpenAI自动配置类
 *
 * @Author sun
 * @Create 2024/12/14 11:39
 * @Version 1.0
 */
@Configuration
@EnableConfigurationProperties({OpenAiProperties.class}) // 启用配置类
@Slf4j
public class OpenAIAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("OpenAIAutoConfiguration has been loaded successfully!");
    }

    /**
     * 创建 OpenAiClient
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public OpenAiClient openAiClient() {
        return new OpenAiClient();
    }
}