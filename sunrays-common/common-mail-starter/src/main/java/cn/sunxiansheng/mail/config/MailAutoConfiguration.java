package cn.sunxiansheng.mail.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 邮件自动配置类
 *
 * @Author sun
 * @Create 2025/1/2 17:05
 * @Version 1.0
 */
@Configuration
@Slf4j
public class MailAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("MailAutoConfiguration has been loaded successfully!");
    }
}