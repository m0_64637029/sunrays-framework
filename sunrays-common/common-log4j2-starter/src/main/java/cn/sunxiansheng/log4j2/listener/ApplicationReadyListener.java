package cn.sunxiansheng.log4j2.listener;

import cn.sunxiansheng.tool.constant.DateParsePatterns;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;

import java.io.File;
import java.net.InetAddress;
import java.util.Arrays;

/**
 * Description: ApplicationReadyListener，表示应用已经完全启动并处于可用状态，常用于触发某些需要在应用完全启动后执行的操作。
 * 动态获取并打印日志存储的根目录的绝对路径、应用的访问地址以及前端可以请求的日期格式。
 *
 * @Author sun
 */
@Slf4j
public class ApplicationReadyListener implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        try {
            // 获取 LoggerContext
            LoggerContext context = (LoggerContext) LogManager.getContext(false);
            // 获取 Log4j2 的当前配置
            Configuration config = context.getConfiguration();

            // 获取 LOG_HOME 属性值
            String logHome = config.getStrSubstitutor().replace("${LOG_HOME}");
            if (logHome == null || logHome.isEmpty()) {
                log.warn("未配置 LOG_HOME 属性！");
            } else {
                // 获取 LOG_HOME 的绝对路径
                File logHomeDir = new File(logHome).getCanonicalFile();
                log.info("日志存储的根目录为: {}", logHomeDir.getAbsolutePath());
            }

            // 获取 Spring 环境
            Environment environment = event.getApplicationContext().getEnvironment();

            // 获取访问地址
            String ip = InetAddress.getLocalHost().getHostAddress();
            String port = environment.getProperty("server.port", "8080");
            String contextPath = environment.getProperty("server.servlet.context-path", "");
            if (!contextPath.startsWith("/")) {
                contextPath = "/" + contextPath;
            }

            // 打印访问路径
            log.info("应用访问地址: http://{}:{}{}", ip, port, contextPath);

            // 打印前端可以请求的日期格式
            log.info("前端可以请求的日期格式: {}", Arrays.toString(DateParsePatterns.PARSE_PATTERNS));

        } catch (Exception e) {
            log.error("获取日志存储根目录、应用访问地址或日期格式时发生错误", e);
        }
    }
}