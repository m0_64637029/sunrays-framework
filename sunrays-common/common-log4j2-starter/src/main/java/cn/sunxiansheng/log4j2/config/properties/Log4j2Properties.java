package cn.sunxiansheng.log4j2.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: 日志切面配置
 *
 * @Author sun
 * @Create 2024/10/24 23:16
 * @Version 1.0
 */
@ConfigurationProperties(prefix = "sun-rays.log4j2")
@Data
public class Log4j2Properties {

    /**
     * 是否开启日志切面
     */
    private boolean logAspectEnable = true;

    /**
     * 日志存储根目录
     */
    private String home = "./logs";
}