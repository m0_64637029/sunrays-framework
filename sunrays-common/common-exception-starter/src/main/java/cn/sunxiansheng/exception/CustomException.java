package cn.sunxiansheng.exception;

import cn.sunxiansheng.tool.response.RespBeanEnum;

/**
 * Description: 自定义异常类
 *
 * @Author sun
 * @Create 2024/5/6 15:15
 * @Version 1.1
 */
public class CustomException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 异常状态码
     */
    private Integer errorCode;

    /**
     * 异常信息
     */
    private String errorMessage;


    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * 重写getMessage方法，返回自定义的异常信息
     *
     * @return
     */
    @Override
    public String getMessage() {
        return this.errorMessage;
    }

    /**
     * 无参构造
     */
    public CustomException() {
    }


    /**
     * 通过错误码和错误信息构造异常
     *
     * @param errorCode
     * @param errorMessage
     */
    public CustomException(Integer errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    /**
     * 通过枚举类构造异常
     *
     * @param respBeanEnum
     */
    public CustomException(RespBeanEnum respBeanEnum) {
        this.errorCode = respBeanEnum.getCode();
        this.errorMessage = respBeanEnum.getMessage();
    }
}