package cn.sunxiansheng.exception.config;

import cn.sunxiansheng.exception.GlobalExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 异常自动配置类
 *
 * @Author sun
 * @Create 2024/10/28 15:37
 * @Version 1.0
 */
@Configuration
@Slf4j
public class ExceptionAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("ExceptionAutoConfiguration has been loaded successfully!");
    }

    /**
     * 条件注入全局异常处理器
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }
}