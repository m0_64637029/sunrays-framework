package cn.sunxiansheng.test.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 测试自动配置类
 *
 * @Author sun
 * @Create 2024/11/12 15:32
 * @Version 1.0
 */
@Configuration
@Slf4j
public class TestAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("TestAutoConfiguration has been loaded successfully!");
    }
}