功能：
1.封装了Minio的通用操作，直接注入MinioUtil就可以直接使用
------------------------------------------------------------------------------------------------------------------------
配置：
sun-rays:
  minio:
    endpoint: ${MINIO_ENDPOINT} # minio服务地址 http://ip:端口
    accessKey: ${MINIO_ACCESS_KEY} # minio服务的accessKey
    secretKey: ${MINIO_SECRET_KEY} # minio服务的secretKey