package cn.sunxiansheng.minio.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: Minio配置
 *
 * @Author sun
 * @Create 2024/11/12 23:15
 * @Version 1.0
 */
@ConfigurationProperties(prefix = "sun-rays.minio")
@Data
public class MinioProperties {

    /**
     * minio服务地址
     */
    private String endpoint;
    /**
     * minio服务用户名
     */
    private String accessKey;
    /**
     * minio服务密码
     */
    private String secretKey;
}