package cn.sunxiansheng.minio.config;

import cn.sunxiansheng.minio.config.properties.MinioProperties;
import cn.sunxiansheng.minio.utils.MinioUtil;
import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: Minio自动配置类
 *
 * @Author sun
 * @Create 2024/11/12 23:14
 * @Version 1.0
 */
@Configuration
@EnableConfigurationProperties({MinioProperties.class}) // 启用配置类
@Slf4j
public class MinioAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("MinioAutoConfiguration has been loaded successfully!");
    }

    /**
     * 注入MinioClient
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    MinioClient minioClient(MinioProperties minioProperties) {
        return MinioClient.builder()
                .endpoint(minioProperties.getEndpoint())
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
                .build();
    }

    /**
     * 注入MinioUtil
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    MinioUtil minioUtil() {
        return new MinioUtil();
    }
}