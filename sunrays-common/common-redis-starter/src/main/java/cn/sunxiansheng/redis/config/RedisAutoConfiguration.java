package cn.sunxiansheng.redis.config;

import cn.sunxiansheng.redis.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Description: Redis自动配置类
 *
 * @Author sun
 * @Create 2024/11/15 20:41
 * @Version 1.0
 */
@Configuration
@Import({RedisConfig.class}) // 导入RedisConfig配置类
@Slf4j
public class RedisAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("RedisAutoConfiguration has been loaded successfully!");
    }

    /**
     * RedisTemplate实例
     */
    @Resource
    RedisTemplate<String, Object> redisTemplate;

    @Bean
    @ConditionalOnMissingBean
    public RString rString() {
        return new RString(redisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    public RHash rHash() {
        return new RHash(redisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    public RSortedSet rSortedSet() {
        return new RSortedSet(redisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    public RList rList() {
        return new RList(redisTemplate);
    }

    @Bean
    @ConditionalOnMissingBean
    public RSet rSet() {
        return new RSet(redisTemplate);
    }
}