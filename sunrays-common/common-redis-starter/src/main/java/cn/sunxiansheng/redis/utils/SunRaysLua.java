package cn.sunxiansheng.redis.utils;

/**
 * Description: 常用的Lua脚本
 * <p>
 * 该接口定义了常用的 Redis Lua 脚本。通过使用 Lua 脚本，可以在 Redis 中执行原子操作，减少网络延迟并提升性能。
 *
 * @Author sun
 * @Create 2025/1/8 17:09
 * @Version 1.0
 */
public interface SunRaysLua {

    /**
     * Lua脚本：获取并删除指定键的值
     * <p>
     * 该脚本首先使用 `GET` 命令获取指定键的值。如果该键存在，返回键的值并删除该键；如果键不存在，则返回 `nil`。
     * 该操作是原子性的，因此可以确保不会在获取值和删除键之间发生并发问题。
     *
     * 参数：
     * KEYS[1]：要操作的 Redis 键（该脚本只支持单个键操作）。
     *
     * 返回值：
     * - 如果指定的键存在，返回键的值并删除该键。
     * - 如果指定的键不存在，返回 `nil`。
     */
    String GET_AND_DELETE =
            "local value = redis.call('GET', KEYS[1]) " +
                    "if value then " +
                    "    redis.call('DEL', KEYS[1]) " +
                    "    return value " +
                    "else " +
                    "    return nil " +
                    "end";
}
