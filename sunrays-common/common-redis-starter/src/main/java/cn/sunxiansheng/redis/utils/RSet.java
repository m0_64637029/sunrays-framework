package cn.sunxiansheng.redis.utils;

import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashSet;
import java.util.Set;

/**
 * Description: Redis的Set类型操作
 *
 * @Author sun
 * @Create 2024/11/14 14:50
 * @Version 1.0
 */
public class RSet extends RBase {

    /**
     * 构造器给父类注入RedisTemplate
     *
     * @param redisTemplate RedisTemplate实例
     */
    public RSet(RedisTemplate<String, Object> redisTemplate) {
        super(redisTemplate);
    }

    /**
     * 添加一个元素
     *
     * @param key  键
     * @param data 值
     * @return 添加成功的个数
     */
    public Long add(String key, Object data) {
        return redisTemplate.opsForSet().add(key, data);
    }

    /**
     * 批量添加元素，并返回添加成功的个数
     *
     * @param key     键
     * @param dataSet 值集合
     * @param <T>     添加的元素类型
     * @return 添加成功的个数
     */
    public <T> Long addBatch(String key, Set<T> dataSet) {
        return redisTemplate.opsForSet().add(key, dataSet.toArray());
    }

    /**
     * 移除特定的元素，并返回移除成功的个数
     *
     * @param key     键
     * @param dataSet 要移除的元素集合
     * @param <T>     要移除的元素类型
     * @return 移除成功的个数
     */
    public <T> Long remove(String key, Set<T> dataSet) {
        return redisTemplate.opsForSet().remove(key, dataSet.toArray());
    }

    /**
     * 获取集合，并将每个元素转换为指定类型
     *
     * @param key  键
     * @param type 集合中元素的目标类型
     * @param <T>  元素的类型
     * @return 转换后的集合
     */
    public <T> Set<T> members(String key, Class<T> type) {
        Set<Object> members = redisTemplate.opsForSet().members(key);
        Set<T> result = new HashSet<>();

        if (members != null) {
            for (Object member : members) {
                result.add(convertValue(member, type));
            }
        }
        return result;
    }

    /**
     * 判断集合中是否有该值
     *
     * @param key  键
     * @param data 值
     * @return 是否包含该值
     */
    public Boolean isMember(String key, Object data) {
        return redisTemplate.opsForSet().isMember(key, data);
    }

    /**
     * 获取set的长度
     *
     * @param key 键
     * @return 集合的长度
     */
    public Long size(String key) {
        return redisTemplate.opsForSet().size(key);
    }
}