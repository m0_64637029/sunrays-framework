package cn.sunxiansheng.rabbitmq.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description: RabbitMQ配置类
 *
 * @Author sun
 * @Create 2025/1/2 14:22
 * @Version 1.0
 */
@Configuration
@AutoConfigureBefore(RabbitAutoConfiguration.class) // 在RabbitAutoConfiguration之前加载，用于覆盖默认的RabbitMQ配置
public class RabbitMQConfig {

    /**
     * 自定义消息转换器，处理JSON和文本消息
     *
     * @return
     */
    @Bean
    public MessageConverter customMessageConverter() {
        return new MessageConverter() {
            private final Jackson2JsonMessageConverter jacksonConverter = new Jackson2JsonMessageConverter();
            private final SimpleMessageConverter simpleMessageConverter = new SimpleMessageConverter();

            /**
             * 发送消息的时候触发
             * @param object
             * @param messageProperties
             * @return
             * @throws RuntimeException
             */
            @Override
            public Message toMessage(Object object, MessageProperties messageProperties) throws RuntimeException {
                if (object instanceof String) {
                    // 如果是字符串，设置为文本类型发送
                    messageProperties.setContentType("text/plain");
                    return simpleMessageConverter.toMessage(object, messageProperties);
                } else {
                    // 默认当作JSON类型处理
                    messageProperties.setContentType("application/json");
                    return jacksonConverter.toMessage(object, messageProperties);
                }
            }

            /**
             * 接收消息的时候触发
             * @param message
             * @return
             * @throws RuntimeException
             */
            @Override
            public Object fromMessage(Message message) throws RuntimeException {
                String contentType = message.getMessageProperties().getContentType();
                // 如果是json类型的数据，就将其自动反序列化为Java对象
                if ("application/json".equals(contentType)) {
                    return jacksonConverter.fromMessage(message);
                    // 如果是文本类型的数据，就将其转换为字符串
                } else if ("text/plain".equals(contentType)) {
                    return simpleMessageConverter.fromMessage(message);
                } else {
                    throw new RuntimeException("自定义的消息转换器不支持该类型: " + contentType);
                }
            }
        };
    }

    /**
     * 设置RabbitTemplate的消息转换器为自定义消息转换器
     *
     * @param connectionFactory
     * @return
     */
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        // 使用自定义消息转换器
        rabbitTemplate.setMessageConverter(customMessageConverter());
        return rabbitTemplate;
    }
}