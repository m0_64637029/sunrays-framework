package cn.sunxiansheng.rabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * Description: RabbitMQ自动配置类
 *
 * @Author sun
 * @Create 2024/12/31 18:44
 * @Version 1.0
 */
@Configuration
@Import(RabbitMQConfig.class)
@Slf4j
public class RabbitMQAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("RabbitMQAutoConfiguration has been loaded successfully!");
    }
}