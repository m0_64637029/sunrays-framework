package cn.sunxiansheng.validation.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: 验证自动配置
 *
 * @Author sun
 * @Create 2024/10/27 17:34
 * @Version 1.0
 */
@Configuration
@Slf4j
public class ValidationAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("ValidationAutoConfiguration has been loaded successfully!");
    }
}