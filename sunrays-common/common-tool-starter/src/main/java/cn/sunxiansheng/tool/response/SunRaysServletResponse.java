package cn.sunxiansheng.tool.response;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 比如在过滤器的时候，如果抛出异常，不会被全局异常处理器处理到，所以采用Servlet的方式响应，跟项目中的效果一致
 */
@Slf4j  // 启用日志功能
public class SunRaysServletResponse {

    // 使用 Gson 来将对象转化为 JSON 字符串
    private static final Gson gson = new Gson();

    public static <T> void writeResponse(HttpServletResponse response, boolean success, int code, String message, T data) throws IOException {
        // 设置响应内容类型为 JSON
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        // 构造 ResultWrapper 对象
        ResultWrapper<T> resultWrapper = new ResultWrapper.Builder<T>()
                .success(success)
                .code(code)
                .message(message)
                .data(data)
                .build();

        // 将 ResultWrapper 对象转化为 JSON 字符串
        String jsonResponse = gson.toJson(resultWrapper);

        // 打印日志
        log.info("SunRaysServletResponse Data: {}", jsonResponse);

        // 将 JSON 响应写入输出流
        response.getWriter().write(jsonResponse);
    }

    // 返回 fail 状态的工具方法
    public static <T> void writeFail(HttpServletResponse response, int code, String message) throws IOException {
        // 打印日志
        log.error("Fail SunRaysServletResponse: code={}, message={}", code, message);

        writeResponse(response, false, code, message, null);
    }

    // 返回 success 状态的工具方法
    public static <T> void writeSuccess(HttpServletResponse response, int code, String message, T data) throws IOException {
        // 打印日志
        log.info("Success SunRaysServletResponse: code={}, message={}, data={}", code, message, data);

        writeResponse(response, true, code, message, data);
    }
}
