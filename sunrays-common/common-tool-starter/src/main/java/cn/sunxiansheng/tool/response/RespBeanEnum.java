package cn.sunxiansheng.tool.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Description: 响应枚举类
 *
 * @Author sun
 * @Create 2024/10/11 18:23
 * @Version 1.0
 */

@Getter
@AllArgsConstructor
@ToString
public enum RespBeanEnum {

    // ============================== 枚举常量 ==============================
    // 通用
    SUCCESS(200, "操作成功"),
    ERROR(500, "系统异常"),
    // 可以在此处添加更多枚举常量，例如：
    // NOT_FOUND(404, "资源未找到"),
    // UNAUTHORIZED(401, "未授权"),
    ;
    // ============================== 枚举常量 ==============================

    // 响应码和响应信息
    private final Integer code;
    private final String message;

    /**
     * 将枚举转换为map（静态初始化）
     */
    public static final Map<Integer, RespBeanEnum> channelEnumMap = Stream.of(RespBeanEnum.values())
            .collect(Collectors.toMap(RespBeanEnum::getCode, Function.identity()));

    /**
     * 根据code来获取枚举
     */
    public static RespBeanEnum getByCode(int code) {
        return channelEnumMap.get(code);
    }
}
