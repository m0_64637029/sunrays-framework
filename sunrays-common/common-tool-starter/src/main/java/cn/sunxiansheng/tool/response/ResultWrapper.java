package cn.sunxiansheng.tool.response;

import lombok.Data;

import java.io.Serializable;

/**
 * Description: 通用响应封装类，通过枚举来获取code和message
 *
 * @Author sun
 * @Create 2024/10/11
 * @Version 1.0
 */
@Data
public class ResultWrapper<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    // 是否成功
    private boolean success;

    // 响应代码
    private int code;

    // 响应消息
    private String message;

    // 响应数据
    private T data;

    // 私有构造器，防止外部直接创建
    private ResultWrapper() {
    }

    // 使用内部建造者类进行对象构建
    public static class Builder<T> {

        private boolean success;

        private int code;

        private String message;

        private T data;

        // ============================== 链式调用设置建造者对象 ==============================
        public Builder<T> success(boolean success) {
            this.success = success;
            return this;
        }

        public Builder<T> code(int code) {
            this.code = code;
            return this;
        }

        public Builder<T> message(String message) {
            this.message = message;
            return this;
        }

        public Builder<T> data(T data) {
            this.data = data;
            return this;
        }
        // ============================== 链式调用设置建造者对象 ==============================

        // ============================== 构建ResultWrapper对象 ==============================
        public ResultWrapper<T> build() {
            ResultWrapper<T> result = new ResultWrapper<>();
            result.success = this.success;
            result.code = this.code;
            result.message = this.message;
            result.data = this.data;
            return result;
        }
        // ============================== 构建ResultWrapper对象 ==============================
    }

    // ============================== 快捷方法 ==============================
    public static <T> ResultWrapper<T> ok() {
        return new Builder<T>()
                .success(true)
                .code(RespBeanEnum.SUCCESS.getCode())
                .message(RespBeanEnum.SUCCESS.getMessage())
                .build();
    }

    public static <T> ResultWrapper<T> ok(T data) {
        return new Builder<T>()
                .success(true)
                .code(RespBeanEnum.SUCCESS.getCode())
                .message(RespBeanEnum.SUCCESS.getMessage())
                .data(data)
                .build();
    }

    public static <T> ResultWrapper<T> ok(T data, String message) {
        return new Builder<T>()
                .success(true)
                .code(RespBeanEnum.SUCCESS.getCode())
                .message(message)
                .data(data)
                .build();
    }

    public static <T> ResultWrapper<T> fail() {
        return new Builder<T>()
                .success(false)
                .code(RespBeanEnum.ERROR.getCode())
                .message(RespBeanEnum.ERROR.getMessage())
                .build();
    }

    public static <T> ResultWrapper<T> fail(String message) {
        return new Builder<T>()
                .success(false)
                .code(RespBeanEnum.ERROR.getCode())
                .message(message)
                .build();
    }

    public static <T> ResultWrapper<T> fail(int code, String message) {
        return new Builder<T>()
                .success(false)
                .code(code)
                .message(message)
                .build();
    }

    public static <T> ResultWrapper<T> fail(int code, String message, T data) {
        return new Builder<T>()
                .success(false)
                .code(code)
                .message(message)
                .data(data)
                .build();
    }

    public static <T> ResultWrapper<T> fail(RespBeanEnum respBeanEnum) {
        return new Builder<T>()
                .success(false)
                .code(respBeanEnum.getCode())
                .message(respBeanEnum.getMessage())
                .build();
    }
    // ============================== 快捷方法 ==============================
}
