package cn.sunxiansheng.tool.constant;

/**
 * Description: 日期解析格式常量类
 *
 * @Author sun
 * @Create 2025/1/3 15:15
 * @Version 1.0
 */
public interface DateParsePatterns {

    String[] PARSE_PATTERNS = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};
}
