功能：
1.JacksonConfig 提供了对日期类型的序列化支持。
2.自定义IgnoredResultWrapper注解，并使用装饰器对SpringMVC的RequestMappingHandlerAdapter进行装饰，实现三种Web响应机制。
3.BaseController 使用@InitBinder，将前台传进的多种日期格式自动反序列化为Date类型。
------------------------------------------------------------------------------------------------------------------------
配置：
1.就可以激活指定的配置文件
spring:
  profiles:
    active: ${env-flag} # 配置为application-xxx.yml中的xxx，就可以激活指定的配置文件
2.pom.xml打包配置
    <!-- maven 打包常规配置 -->
    <build>
        <!-- 打包成 jar 包时的名字为项目的artifactId + version -->
        <finalName>${project.artifactId}-${project.version}</finalName>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <!-- 引用父模块中统一管理的插件版本（与SpringBoot的版本一致！ -->
                <executions>
                    <execution>
                        <goals>
                            <!-- 将所有的依赖包都打到这个模块中 -->
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>