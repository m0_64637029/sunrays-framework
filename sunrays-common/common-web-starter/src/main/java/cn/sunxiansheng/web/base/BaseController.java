package cn.sunxiansheng.web.base;

import cn.sunxiansheng.tool.response.RespBeanEnum;
import cn.sunxiansheng.tool.response.ResultWrapper;
import cn.sunxiansheng.web.utils.DateUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;

/**
 * Description: Controller层基类
 *
 * @Author sun
 * @Create 2024/10/26 09:48
 * @Version 1.0
 */
public class BaseController {

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     * 注意：只对表单参数（application/x-www-form-urlencoded）或查询参数生效，对于json格式的请求不生效
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 成功
     *
     * @return
     */
    public ResultWrapper<Object> ok() {
        return ResultWrapper.ok();
    }

    /**
     * 失败
     *
     * @return
     */
    public ResultWrapper<Object> fail() {
        return ResultWrapper.fail();
    }

    /**
     * 失败，自定义消息
     *
     * @param message
     * @return
     */
    public ResultWrapper<Object> fail(String message) {
        return ResultWrapper.fail(message);
    }

    /**
     * 失败，自定义状态码和消息
     *
     * @param code
     * @param message
     * @return
     */
    public ResultWrapper<Object> fail(int code, String message) {
        return ResultWrapper.fail(code, message);
    }

    /**
     * 失败，自定义枚举
     *
     * @param respBeanEnum
     * @return
     */
    public ResultWrapper<Object> fail(RespBeanEnum respBeanEnum) {
        return ResultWrapper.fail(respBeanEnum);
    }

    /**
     * 根据影响行数返回成功或者失败
     *
     * @param rows
     * @return
     */
    public ResultWrapper<Object> toRes(int rows) {
        return rows > 0 ? ok() : fail();
    }

    /**
     * 根据布尔值返回成功或者失败
     *
     * @param result
     * @return
     */
    public ResultWrapper<Object> toRes(boolean result) {
        return result ? ok() : fail();
    }
}