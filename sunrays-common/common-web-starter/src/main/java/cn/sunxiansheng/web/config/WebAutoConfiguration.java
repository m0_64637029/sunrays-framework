package cn.sunxiansheng.web.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * Description: Web自动配置类
 *
 * @Author sun
 * @Create 2024/10/26 17:28
 * @Version 1.0
 */
@Configuration
@Import({JacksonConfig.class}) // 导入JacksonConfig配置类
@Slf4j
public class WebAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("WebAutoConfiguration has been loaded successfully!");
    }

    /**
     * 注入对返回结果增强的装饰器
     *
     * @return
     */
    @Bean
    @ConditionalOnMissingBean
    public ReturnValueHandlersDecorator returnValueHandlersDecorator() {
        return new ReturnValueHandlersDecorator();
    }
}