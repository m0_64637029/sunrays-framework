package cn.sunxiansheng.env.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Description: 这里的path只是为了代码提示，实际上DotenvEnvironmentPostProcessor.java不从这里获取配置
 *
 * @Author sun
 * @Create 2025/1/10 20:04
 * @Version 1.0
 */
@ConfigurationProperties(prefix = "sun-rays.env")
@Data
public class EnvProperties {

    /**
     * .env文件的绝对路径
     */
    private String path;
}