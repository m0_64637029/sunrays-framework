package cn.sunxiansheng.env.processor;

import io.github.cdimascio.dotenv.Dotenv;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * Description: 在${xxx}解析之前执行，可以读取xxx，设置到环境中，在后续的解析时就会进行替换了
 *
 * @Author sun
 * @Create 2025/1/10 19:40
 * @Version 1.0
 */
public class DotenvEnvironmentPostProcessor implements EnvironmentPostProcessor{

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        // 从 Spring 的配置中获取 sun-rays.env.path
        String dotenvPath = environment.getProperty("sun-rays.env.path");

        if (dotenvPath != null) {
            // 加载 .env 文件
            Dotenv dotenv = Dotenv.configure()
                    .directory(dotenvPath)
                    .filename(".env")
                    .load();

            // 将 .env 中的值注入到系统属性中，确保占位符解析时可用
            dotenv.entries().forEach(entry ->
                    environment.getSystemProperties().put(entry.getKey(), entry.getValue())
            );

            System.out.println("Loaded .env from path: " + dotenvPath);
        } else {
            System.err.println("sun-rays.env.path not configured！");
        }
    }
}