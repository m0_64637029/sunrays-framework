package cn.sunxiansheng.env.config;

import cn.sunxiansheng.env.config.properties.EnvProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: Env模块自动配置类
 *
 * @Author sun
 * @Create 2025/1/10 19:57
 * @Version 1.0
 */
@Configuration
@EnableConfigurationProperties({EnvProperties.class}) // 启用配置类
@Slf4j
public class EnvAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("EnvAutoConfiguration has been loaded successfully!");
    }
}