package cn.sunxiansheng.cloud.nacos.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: Nacos自动配置
 *
 * @Author sun
 * @Create 2025/1/7 16:41
 * @Version 1.0
 */
@Configuration
@Slf4j
public class NacosAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("NacosAutoConfiguration has been loaded successfully!");
    }
}