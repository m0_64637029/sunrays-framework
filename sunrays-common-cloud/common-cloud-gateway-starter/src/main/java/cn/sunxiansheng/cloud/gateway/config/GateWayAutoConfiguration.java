package cn.sunxiansheng.cloud.gateway.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: GateWay自动配置类
 *
 * @Author sun
 * @Create 2025/1/7 17:19
 * @Version 1.0
 */
@Slf4j
@Configuration
public class GateWayAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("GateWayAutoConfiguration has been loaded successfully!");
    }
}