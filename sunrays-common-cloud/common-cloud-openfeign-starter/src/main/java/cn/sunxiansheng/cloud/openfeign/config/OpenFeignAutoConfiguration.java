package cn.sunxiansheng.cloud.openfeign.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * Description: OpenFeign 自动配置类
 *
 * @Author sun
 * @Create 2025/1/8 12:13
 * @Version 1.0
 */
@Configuration
@Slf4j
public class OpenFeignAutoConfiguration {

    /**
     * 自动配置成功日志
     */
    @PostConstruct
    public void logConfigSuccess() {
        log.info("OpenFeignAutoConfiguration has been loaded successfully!");
    }
}